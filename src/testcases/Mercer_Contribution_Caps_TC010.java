package testcases;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utilities.Abstract;
import utilities.BaseClass;
import utilities.SupportiveMethods;
import utilities.WrapperFunctions;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;


public class Mercer_Contribution_Caps_TC010 extends BaseClass {
	static WebDriver driver=null;
	static Properties OR=null;
	static String testScore="unset";
	WrapperFunctions wrapper;
	List<String>dashboardElementNames=new ArrayList<String>();
	//Set Property for ATU Reporter Configuration
	{
		System.out.println("Getting config files..");
		System.setProperty("atu.reporter.config",Abstract.ATUconfigPath);
		System.out.println("Config read successfull");
	}


	@DataProvider(name="TestDataProvider")
	public Object[][] getData() throws Exception
	{
		//Loading all properties files to POJO
		this.initializeEnvironment();
		String env=getProjectConfig().getProperty("environment");
		String excelpath=Abstract.TestDataPath+"//TestCases"+"_"+env+".xlsx";
		System.out.println("Excel Path: "+excelpath);
		//Passing test case name and test case path for getting TestData
		Object[][] testData = loadDataProvider("Mercer_Contribution_Caps_TC010",excelpath,"TestData");
		return testData;	
	}


	@Test(dataProvider="TestDataProvider")
	public void TC010_Mercer_Contribution_Caps(Hashtable<String,String>testData) throws Exception
	{

		loadTestData(testData);
		//Driver Object
		driver=getDriver();
		OR=SupportiveMethods.ObjectRepository();
		wrapper=new WrapperFunctions(this);

		System.out.println("**********************Ececution STarts Here*********************");
		System.out.println("Browser Name: "+dpString("Browser"));	
		System.out.println("Testing site: "+dpString("Link"));
		ATUReports.add("Mercer Australia Public Home Page", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
		System.out.println("Title: "+driver.getTitle());
		SecurePageLogin.secureLogin_Stage(driver);
		System.out.println("Logged In Successfully...");
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver,300);
		wrapper.click(driver.findElement(By.xpath("//a[text()='ACAST']")));	

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("TopazWebPartManager1_twp616000635_txtMemberNumber")).clear();
		driver.findElement(By.id("TopazWebPartManager1_twp616000635_txtMemberNumber")).sendKeys("501");
		//driver.findElement(By.xpath("//*[@id=TopazWebPartManager1_twp616000635_txtMemberNumber]")).clear();
		//driver.findElement(By.xpath("//*[@id=TopazWebPartManager1_twp616000635_txtMemberNumber]")).sendKeys("501");

		

		driver.findElement(By.name("ctl00$TopazWebPartManager1$twp616000635$ctl03")).click();

		//SupportiveMethods.waitMethod(OR.getProperty("GreetingText"));

		wait.until(ExpectedConditions.elementToBeClickable(By.id("sample")));

		try{
			if(driver.findElement(By.id("sample")).isDisplayed())
			{
				wait.until(ExpectedConditions.elementToBeClickable(By.id("sample")));
				driver.findElement(By.id("sample")).click();
			}
		}catch(Exception e)
		{
			System.out.println("Pop up not displayed...");
		}
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[3]/img")));
		//driver.manage().addCookie(ck);

		//SupportiveMethods.waitMethod(OR.getProperty("HandBurgerMenu"));
		wrapper.click("HandBurgerMenu");
		//***********************************************************************************
		wrapper.click("ContributionCaps");
		//Verifying Static Contents

		wrapper.checkElementDisplyed(driver.findElement(By.xpath("//h1[text()='ANNUAL CONTRIBUTION CAPS']")));

		wrapper.checkElementDisplyed(driver.findElement((By.xpath("//h3[text()='Current year contribution caps']"))));

		wrapper.checkElementDisplyed(driver.findElement((By.xpath("//div[@class='current-year']"))));

		wrapper.checkElementDisplyed(driver.findElement(By.xpath("//h3[@class='CurYearheading global-heading4']")));

		wrapper.click(driver.findElement(By.xpath("//h3[@class='CurYearheading global-heading4']/i")));

		wrapper.checkElementDisplyed(driver.findElement((By.xpath("//div[@class='previous-year']"))));

		//driver.quit();		
	}
}
