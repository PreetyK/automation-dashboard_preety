/*
 * Copyright (c) 2014 ContinuumSecurity www.continuumsecurity.net
 *
 * The contents of this file are subject to the GNU Affero General Public
 * License version 3 (the "License"); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * http://www.gnu.org/licenses/agpl-3.0.txt
 *
 * Software distributed under the License is distributed on an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific language governing rights and limitations
 * under the License.
 *
 * The Initial Developer of the Original Code is ContinuumSecurity.
 * Portions created by ContinuumSecurity are Copyright (C)
 * ContinuumSecurity SLNE. All Rights Reserved.
 *
 * Contributor(s): Stephen de Vries
 */

package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import utilities.BaseClass;


public class MercerLogin_Security extends BaseClass {
	WebDriver driver;
	final static String BASE_URL ="https://secure.uat.superfacts.com/web/mst/home.tpz";
	final static String PUBLIC_URL ="http://stg.mercerfinancialservices.com/";	
	final static String USERNAME = "adithyan.m@mercer.com";
	final static String PASSWORD = "Aadhi@123";

	public MercerLogin_Security(WebDriver driver) {
		this.driver = driver;
		this.driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
		this.driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
	}

	public void MercerLogin_SecurityPublic(WebDriver driver) {
		/*this.driver = driver;
		this.driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
		this.driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);*/
		driver.get(PUBLIC_URL);
	}
	public void login() throws Exception {
		System.out.println("Login test");
		driver.get(BASE_URL);
	
		driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtCompanyName_txtCompany']")).clear();
		driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtCompanyName_txtCompany']")).sendKeys("100001");

		driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtUserName']")).clear();
		driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtUserName']")).sendKeys("2222");

		driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtPassword']")).clear();
		driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtPassword']")).sendKeys("4004");

		driver.findElement(By.xpath("//*[@id='submitButton']/input")).click();
		Thread.sleep(5000);
		  }
	public void verifyTextPresent(String text) {
		if (!this.driver.getPageSource().contains(text)) throw new RuntimeException("Expected text: ["+text+"] was not found.");
	}
}
