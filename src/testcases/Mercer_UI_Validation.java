package testcases;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utilities.Abstract;
import utilities.BaseClass;
import utilities.SupportiveMethods;
import utilities.WrapperFunctions;



public class Mercer_UI_Validation extends BaseClass {
	WebDriver driver=null;
	List<String>whatWeDoNames=new ArrayList<String>();
	List<String>whoWeHelpNames=new ArrayList<String>();
	List<String>ourThinkingNames=new ArrayList<String>();
	List<String>ourCompanyNames=new ArrayList<String>();
	static String testScore="unset";
	Properties objRepository;
	WrapperFunctions wrapper;
	//Set Property for ATU Reporter Configuration
	{
		System.out.println("Getting config files..");
		System.setProperty("atu.reporter.config",Abstract.ATUconfigPath);
		System.out.println("Config read successfull");
	}
	
	@DataProvider(name="TestDataProvider")
	public Object[][] getData() throws Exception
	{
		//Loading all properties files to POJO
		this.initializeEnvironment();
		String env=getProjectConfig().getProperty("environment");
		String excelpath=Abstract.TestDataPath+"//TestCases"+"_"+env+".xlsx";
		System.out.println("Excel Path: "+excelpath);
		//Passing test case name and test case path for getting TestData
		Object[][] testData = loadDataProvider("Mercer_UI_Validation",excelpath,"UI Validation");
		return testData;	
	}


	@Test(dataProvider="TestDataProvider")
	public void MercerForm_SpeakWithConsultant_TC001(Hashtable<String,String>testData) throws Exception
	{
		//Loading TestData to POJO
		loadTestData(testData);
		//Driver Object
		driver=getDriver();
		driver.manage().window().maximize();
		objRepository=getObjectRepository();
		wrapper=new WrapperFunctions(this);
		try{
			System.out.println("**********************Ececution STarts Here*********************");
			String browser=dpString("BrowserName");
			System.out.println("Browser Name: "+dpString("BrowserName"));	
			System.out.println("Testing site: "+dpString("Link"));
			System.out.println("Title: "+driver.getTitle());
			wrapper.waitForElement(driver.findElement(By.id("submit")),10);
			driver.findElement(By.id("submit")).click();
			Thread.sleep(2000);
			SupportiveMethods.compareScreen(driver.getTitle().replace('|', '_'),browser);
	//	SupportiveMethods.logReporter(driver.getTitle(),"","Checking For "+driver.getTitle()+"Visual Validation ",driver.getTitle()+"validated Successfully",SupportiveMethods.compareScreen(driver.getTitle().replace('|', '_'),browser));		
		System.out.println("Title: "+driver.getTitle());
		//***********************************************************************************
		//Navigation Scripts
		//Who We Help
		Actions a=new Actions(driver);
		//SupportiveMethods.waitMethod(objRepository.getProperty("WhatWeDoLink"));
		a.moveToElement(driver.findElement(By.xpath(objRepository.getProperty("WhatWeDoLink")))).build().perform();
		Thread.sleep(5000);
		List<WebElement>whatWeDoElements=driver.findElements(By.xpath(objRepository.getProperty("WhatWeDoSubLinks")));
		System.out.println("No of Elements: "+whatWeDoElements.size());

		for(int i=0;i<whatWeDoElements.size();i++)
		{
			String temp=whatWeDoElements.get(i).getText();
			System.out.println(temp);
			whatWeDoNames.add(temp);
		}

		for(int i=0;i<whatWeDoNames.size();i++)
		{	
			SupportiveMethods.waitMethod(objRepository.getProperty("WhatWeDoLink"));
			a.moveToElement(driver.findElement(By.xpath(objRepository.getProperty("WhatWeDoLink")))).build().perform();
			//Thread.sleep(2000);
			String expectedLink=driver.findElement(By.xpath("//a[contains(text(),'"+whatWeDoNames.get(i)+"')]")).getAttribute("href");
			System.out.println("*******************************************************");
			System.out.println("Expected Link: "+expectedLink);
			driver.findElement(By.xpath("//a[contains(text(),'"+whatWeDoNames.get(i)+"')]")).click();	
			driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
			driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
			System.out.println("Title: "+driver.getTitle());
			String actualLink=driver.getCurrentUrl();
			System.out.println("Actual Link: "+driver.getCurrentUrl());
			if(actualLink.contains(expectedLink))
			{
				System.out.println("Redirecting Properly");
			}
			else
			{
				System.out.println("####Check URL####");
			}
			System.out.println("*******************************************************");
			SupportiveMethods.logReporter(driver.getTitle(),"","Checking For"+driver.getTitle()+"Visual Validation",driver.getTitle()+"validated Successfully",SupportiveMethods.compareScreen(driver.getTitle().replace('|', '_'),browser));
			testScore = "pass";
		}
		}
		catch(AssertionError ae) {
			// if we have an assertion error, take a snapshot of where the test fails
			// and set the score to "fail"
			/*SupportiveMethods s=getSupportiveMethodFunctions();
			System.out.println("Inside catch block..");  
			String snapshotHash = s.takeSnapshot(this.getRemoteSessionId());
			s.setDescription(this.getRemoteSessionId(),snapshotHash, ae.toString());
			testScore = "fail";*/
		} /*finally {
			System.out.println("Inside Finally....");
			SupportiveMethods s=getSupportiveMethodFunctions();
			// here we make an api call to actually send the score 
			System.out.println("Test score:"+testScore);
			s.setScore(this.getRemoteSessionId(),testScore);
			// and quit the driver
			driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
			driver.quit();
		}*/
	}
}
