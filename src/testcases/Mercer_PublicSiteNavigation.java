package testcases;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utilities.Abstract;
import utilities.BaseClass;
import utilities.SupportiveMethods;
import utilities.WrapperFunctions;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;


public class Mercer_PublicSiteNavigation extends BaseClass {
	static WebDriver driver=null;
	static Properties OR=null;
	List<String>superNames=new ArrayList<String>();
	List<String>adviceNames=new ArrayList<String>();
	List<String>retirementNames=new ArrayList<String>();
	List<String>mercerMagazineNames=new ArrayList<String>();
	static String testScore="unset";
	WrapperFunctions wrapper;
	List<String>dashboardElementNames=new ArrayList<String>();
	//Set Property for ATU Reporter Configuration
	{
		System.out.println("Getting config files..");
		System.setProperty("atu.reporter.config",Abstract.ATUconfigPath);
		System.out.println("Config read successfull");
	}


	@DataProvider(name="TestDataProvider")
	public Object[][] getData() throws Exception
	{
		//Loading all properties files to POJO
		this.initializeEnvironment();
		String env=getProjectConfig().getProperty("environment");
		String excelpath=Abstract.TestDataPath+"//TestCases"+"_"+env+".xlsx";
		System.out.println("Excel Path: "+excelpath);
		//Passing test case name and test case path for getting TestData
		Object[][] testData = loadDataProvider("Mercer_Aus_BrokenLinks_TC007",excelpath,"UI Verification");
		return testData;	
	}


	@Test(dataProvider="TestDataProvider")
	public void mercerHomeNavigation(Hashtable<String,String>testData) throws Exception
	{
		loadTestData(testData);
		//Driver Object
		driver=getDriver();
		OR=SupportiveMethods.ObjectRepository();
		wrapper=new WrapperFunctions(this);

		System.out.println("**********************Ececution STarts Here*********************");
		String browser=dpString("Browser");
		System.out.println("Browser Name: "+dpString("Browser"));	
		System.out.println("Testing site: "+dpString("Link"));
		ATUReports.add("Mercer Australia Public Home Page", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
		System.out.println("Title: "+driver.getTitle());
		
		
		
		
		//***********************************************************************************
		//Navigation Scripts
		//Super Tab
		Actions a=new Actions(driver);
		a.moveToElement(driver.findElement(By.xpath(OR.getProperty("SuperTab")))).click().perform();
		Thread.sleep(5000);
		List<WebElement>superTabElements=driver.findElements(By.xpath(OR.getProperty("superTabSubLinks")));
		System.out.println("No of Elements: "+superTabElements.size());

		for(int i=0;i<superTabElements.size();i++)
		{
			String temp=superTabElements.get(i).getText();
			System.out.println("Text is:"+temp);
			superNames.add(temp);
		}

		for(int i=0;i<superNames.size();i++)
		{		
			Thread.sleep(2000);
			
			String expectedLink=driver.findElement(By.xpath("//a[contains(text(),\""+superNames.get(i)+"\")]")).getAttribute("href");
			System.out.println("*******************************************************");
			System.out.println("Expected Link: "+expectedLink);
			wrapper.click(driver.findElement(By.xpath("//a[contains(text(),\""+superNames.get(i)+"\")]")));
			//driver.findElement(By.xpath("//a[contains(text(),\""+superNames.get(i)+"\")]")).click();		
			System.out.println("Title: "+driver.getTitle());
			String actualLink=driver.getCurrentUrl();
			System.out.println("Actual Link: "+driver.getCurrentUrl());
			if(actualLink.contains(expectedLink))
			{
				ATUReports.add(driver.getTitle(),actualLink,expectedLink,LogAs.PASSED,new CaptureScreen(ScreenshotOf.DESKTOP));
				System.out.println("Redirecting Properly");
			}
			else
			{
				ATUReports.add(driver.getTitle(),actualLink,expectedLink,LogAs.WARNING,new CaptureScreen(ScreenshotOf.DESKTOP));
				System.out.println("####Check URL####");
			}
			System.out.println("*******************************************************");
			SupportiveMethods.logReporter(driver.getTitle(),"","Checking For"+driver.getTitle()+"Visual Validation",driver.getTitle()+"validated Successfully",SupportiveMethods.compareScreen(driver.getTitle().replace('|', '_'),browser));
			SupportiveMethods.waitMethod(OR.getProperty("SuperTab"));
			a.moveToElement(driver.findElement(By.xpath(OR.getProperty("SuperTab")))).click().perform();

		}
		//***********************************************************************************
		//Advice Tab Verification
		System.out.println("Advice Tab Starts here");
		Actions a1=new Actions(driver);
		//	SupportiveMethods.waitMethod(OR.getProperty("AdviceTab"));
		a1.moveToElement(driver.findElement(By.xpath(OR.getProperty("AdviceTab")))).click().perform();
		Thread.sleep(5000);
		List<WebElement>adviceTabElements=driver.findElements(By.xpath(OR.getProperty("AdviceTabSubLinks")));
		System.out.println("No of Elements in Advice Tab: "+adviceTabElements.size());

		for(int i=0;i<adviceTabElements.size();i++)
		{
			String temp=adviceTabElements.get(i).getText();
			//System.out.println("Text is:"+temp);
			adviceNames.add(temp);
		}

		for(int i=0;i<adviceNames.size();i++)
		{	

			Thread.sleep(2000);
			String expectedLink=driver.findElement(By.xpath("//a[contains(text(),\""+adviceNames.get(i)+"\")]")).getAttribute("href");
			System.out.println("*******************************************************");
			System.out.println("Expected Link: "+expectedLink);
			try{
				//SupportiveMethods.waitMethod("//a[contains(text(),\""+adviceNames.get(i)+"\")]");
				wrapper.click(driver.findElement(By.xpath("//a[contains(text(),\""+adviceNames.get(i)+"\")]")));
				//driver.findElement(By.xpath("//a[contains(text(),\""+adviceNames.get(i)+"\")]")).click();		
			}
			catch(Throwable t)
			{
				System.out.println("Not navigated to"+driver.getCurrentUrl());
			}
			System.out.println("Title: "+driver.getTitle());
			String actualLink=driver.getCurrentUrl();
			System.out.println("Actual Link: "+driver.getCurrentUrl());
			if(actualLink.contains(expectedLink))
			{
				ATUReports.add(driver.getTitle(),actualLink,expectedLink,LogAs.PASSED,new CaptureScreen(ScreenshotOf.DESKTOP));
				System.out.println("Redirecting Properly");
			}
			else
			{
				ATUReports.add(driver.getTitle(),actualLink,expectedLink,LogAs.WARNING,new CaptureScreen(ScreenshotOf.DESKTOP));
				System.out.println("####Check URL####");
			}
			System.out.println("*******************************************************");
			ATUReports.add(driver.getTitle(), LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			SupportiveMethods.logReporter(driver.getTitle(),"","Checking For"+driver.getTitle()+"Visual Validation",driver.getTitle()+"validated Successfully",SupportiveMethods.compareScreen(driver.getTitle().replace('|', '_'),browser));
			SupportiveMethods.waitMethod(OR.getProperty("AdviceTab"));
			a.moveToElement(driver.findElement(By.xpath(OR.getProperty("AdviceTab")))).click().perform();
		}
		driver.close();
		driver.quit();
		//SupportiveMethods.closeBrowser(driver);
	}
}
