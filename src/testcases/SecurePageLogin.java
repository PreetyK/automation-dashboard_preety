package testcases;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import utilities.Pojo;
import utilities.SupportiveMethods;



public class SecurePageLogin extends Pojo {

	//WrapperFunctions wrapper;
  public static WebDriver secureLogin(WebDriver driver) throws Exception {
	  Select environment=new Select(driver.findElement(By.xpath("//select[@id='MainContent_ddlEnv']")));
	  environment.selectByVisibleText("Local to Dev");
	  SupportiveMethods.waitMethod("//select[@id='MainContent_ddlDataCategory']");
	  Select dataCategory=new Select(driver.findElement(By.xpath("//select[@id='MainContent_ddlDataCategory']")));
	  dataCategory.selectByVisibleText("AU");
	  
	  SupportiveMethods.waitMethod("//select[@id='MainContent_ddlClientCode']");
	  Select clientCode=new Select(driver.findElement(By.xpath("//select[@id='MainContent_ddlClientCode']")));
	  clientCode.selectByVisibleText("MST");
	  
	  driver.findElement(By.xpath("//input[@id='MainContent_tbDSNFundID']")).clear();
	  driver.findElement(By.xpath("//input[@id='MainContent_tbDSNFundID']")).sendKeys("3888");
	  
	  driver.findElement(By.xpath("//input[@id='MainContent_tbDBCode']")).clear();
	  driver.findElement(By.xpath("//input[@id='MainContent_tbDBCode']")).sendKeys("MASTA");
	  
	  driver.findElement(By.xpath("//input[@id='MainContent_tbFundCode']")).clear();
	  driver.findElement(By.xpath("//input[@id='MainContent_tbFundCode']")).sendKeys("MT888");
	  
	  driver.findElement(By.xpath("//input[@id='MainContent_tbMemberNumber']")).clear();
	  driver.findElement(By.xpath("//input[@id='MainContent_tbMemberNumber']")).sendKeys("17469");
	
	  driver.findElement(By.xpath("//input[@id='MainContent_tbAEMUrl']")).clear();
	  driver.findElement(By.xpath("//input[@id='MainContent_tbAEMUrl']")).sendKeys("http://mercerdev2.www.marshinc.net/secure");
	  
	  driver.findElement(By.xpath("//input[@value='Start / Refresh Session']")).click();
	  driver.findElement(By.xpath("//input[@value='Open AEM Site']")).click();
	  Thread.sleep(10000);
	  driver.manage().timeouts().pageLoadTimeout(3000,TimeUnit.MILLISECONDS);
	  System.out.println("Title:"+driver.getTitle());
	  try
	  {
	  Assert.assertEquals(driver.getTitle(),"Home Page");
	  }
	  catch(Exception e)
	  {
		  
	  }
	  return driver;
  }
  
  public static WebDriver secureLogin_Stage(WebDriver driver) throws Exception {
	 
	  //wrapper=new WrapperFunctions(this);
	  SupportiveMethods.logReporter("","","User should able to launch the application","User launched the application successfully",true);
	  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtCompanyName_txtCompany']")).clear();
	  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtCompanyName_txtCompany']")).sendKeys("999999");
	  SupportiveMethods.logReporter("","","User should able enter the Employer/Plan No","User entered the Employer/Plan no successfully",true);
		
	  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtUserName']")).clear();
	  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtUserName']")).sendKeys("mcr-mst");
	  SupportiveMethods.logReporter("","","User should able enter the Member number","User entered the Member number successfully",true);
		
	  
	  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtPassword']")).clear();
	  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtPassword']")).sendKeys("1111");
	  SupportiveMethods.logReporter("","","User should able enter the PIN/Password","User entered the PIN/Password successfully",true);
		
	  
	  driver.findElement(By.xpath("//input[@name='ctl00$TopazWebPartManager1$twp1783717138$ctl01']")).click();
	  SupportiveMethods.logReporter("","","User should able to click the submit button","User clicked the submit button successfully successfully",true);
		
	 // driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp435268533_hyplnkNewSite']")).click();
	  driver.manage().timeouts().pageLoadTimeout(180,TimeUnit.SECONDS);
	 	  
	  System.out.println("Title:"+driver.getTitle());
	  
	  return driver;
  }
 
}
