package testcases;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.concurrent.*;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.*;
import org.testng.*;
import org.testng.annotations.*;

import atu.testng.reports.*;
import atu.testng.reports.logging.*;
import atu.testng.selenium.reports.*;
import atu.testng.selenium.reports.CaptureScreen.*;
import utilities.*;


/**
 * The Mercer_Aus_Investments_TC004 program checks for users Investment details
 *
 * @author  Adithyan M
 * @version 1.0
 * @since   2016-06-20 
 */
public class Mercer_Aus_Beneficiaries_TC006 extends BaseClass{
	static WebDriver driver=null;
	static Properties OR=null;
	WrapperFunctions wrapper;
	private String winHandleBefore;
	String memberNumber="501";
	//Set Property for ATU Reporter Configuration
	{
		System.out.println("Getting config files..");
		System.setProperty("atu.reporter.config",Abstract.ATUconfigPath);
		System.out.println("Config read successfull");
	}

	/**
	 * Data provider is used to get multiple data from 
	 * Excel sheet using testcase name and store it into
	 * Hashtable with key and value 
	 */
	@DataProvider(name="TestDataProvider")
	public Object[][] getData() throws Exception
	{
		//Loading all properties files to POJO
		this.initializeEnvironment();
		String env=getProjectConfig().getProperty("environment");
		String excelpath=Abstract.TestDataPath+"//TestCases"+"_"+env+".xlsx";
		System.out.println("Excel Path: "+excelpath);
		//Passing test case name and test case path for getting TestData
		Object[][] testData = loadDataProvider("Mercer_Aus_Beneficiaries_TC006",excelpath,"TestData");
		return testData;	
	}

	/**
	 * Mercer Home page Navigation test steps 
	 *  
	 */
	@Test(dataProvider="TestDataProvider")
	public void mercerHomeNavigation(Hashtable<String,String>testData) throws Exception
	{
		loadTestData(testData);
		//Driver Object
		driver=getDriver();
		OR=getObjectRepository();
		wrapper=new WrapperFunctions(this);

		System.out.println("**********************Ececution STarts Here*********************");
		System.out.println("Browser Name: "+dpString("Browser"));	
		System.out.println("Testing site: "+dpString("Link"));
		ATUReports.add("Mercer Australia Public Home Page", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
		System.out.println("Title: "+driver.getTitle());
		SecurePageLogin.secureLogin_Stage(driver);
		System.out.println("Logged In Successfully...");
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver,300);
		wrapper.click(driver.findElement(By.xpath("//a[text()='ACAST']")));	

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("TopazWebPartManager1_twp616000635_txtMemberNumber")).clear();

		driver.findElement(By.id("TopazWebPartManager1_twp616000635_txtMemberNumber")).sendKeys(memberNumber);


		//driver.findElement(By.xpath("//*[@id=TopazWebPartManager1_twp616000635_txtMemberNumber]")).clear();
		//driver.findElement(By.xpath("//*[@id=TopazWebPartManager1_twp616000635_txtMemberNumber]")).sendKeys("501");



		driver.findElement(By.name("ctl00$TopazWebPartManager1$twp616000635$ctl03")).click();

		//SupportiveMethods.waitMethod(OR.getProperty("GreetingText"));
		try{
			if(driver.findElement(By.id("sample")).isDisplayed())
			{
				wait.until(ExpectedConditions.elementToBeClickable(By.id("sample")));
				driver.findElement(By.id("sample")).click();
			}
		}catch(Exception e)
		{
			System.out.println("Pop up not displayed...");
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[3]/img")));
		wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[3]/img")));
		//wrapper.click("HandBurgerMenu");
		ATUReports.add(driver.getTitle(),"Clicking on Hand Burger Menu","Hand Burger Menu clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
		//	driver.findElement(By.xpath(OR.getProperty("Beneficiaries"))).click();

		wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[2]/div[2]/div/div[6]/a[4]")));
		//wrapper.click("Beneficiaries");
		System.out.println(driver.getWindowHandles().size());
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}
		System.out.println(driver.getTitle());
		//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

		wrapper.checkElementDisplyed(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[1]/div/h1")));
		ATUReports.add(driver.getTitle(),"Validating Page Header NOMINATE BEBEFICIARIES","Page Header NOMINATE BEBEFICIARIES validated successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));

		//verify Preferred Beneficiaries link

		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[2]/div/p[3]/a/span")));
		//wrapper.click(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[2]/div/p[3]/a/span")));
		//ATUReports.add(driver.getTitle(),"Clicking on Preferred Beneficiaries Link","Preferred Beneficiaries Link clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
		//wrapper.click(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[3]/div/div/div[1]/i")));


		//verify Binding Nomination link

		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[2]/div/p[4]/a[1]/span")));
		//wrapper.click(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[2]/div/p[4]/a[1]/span")));
		//ATUReports.add(driver.getTitle(),"Clicking on Binding Nomination Link","Binding Nomination Link clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
		//wrapper.click(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[4]/div/div/div[1]/i")));



		wrapper.checkElementDisplyed(driver.findElement(By.xpath(OR.getProperty("BeneficiariesEditButton"))));
		wrapper.click("BeneficiariesEditButton");

		//SupportiveMethods.waitMethodId(OR.getProperty("BeneficiariesEditButton"));
		//driver.findElement(By.id(OR.getProperty("BeneficiariesEditButton"))).click();

		wrapper.click(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[2]/tr[1]/td[1]/a/b")));	

		//validation if percentage is less than 100


		driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[1]/tr/td[1]/input[2]")).clear();
		driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[1]/tr/td[1]/input[2]")).sendKeys("Jacob");

		driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[1]/tr/td[2]/select/option[3]")).click();
		//wrapper.click(driver.findElement(By.xpath("//div[text()='Child']")));

		driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[1]/tr/td[3]/div/input")).clear();
		driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[1]/tr/td[3]/div/input")).sendKeys("70");


		wrapper.click(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[2]/tr[1]/td[1]/a/b")));	

		driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[1]/tr[2]/td[1]/input[2]")).clear();
		driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[1]/tr[2]/td[1]/input[2]")).sendKeys("Sherlok");

		driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[1]/tr[2]/td[2]/select/option[4]")).click();
		//wrapper.click(driver.findElement(By.xpath("//div[text()='Child']")));

		driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[1]/tr[2]/td[3]/div/input")).clear();
		driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[6]/div/div/table/tbody[1]/tr[2]/td[3]/div/input")).sendKeys("30");



		wrapper.checkElementDisplyed(driver.findElement(By.xpath(OR.getProperty("BeneficiariesSubmitButton"))));
		wrapper.click("BeneficiariesSubmitButton");


		// wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modal-footer"))).click();

		wrapper.javaScriptClick(driver.findElement(By.xpath("//button[text()='Close']")));

		//wrapper.click(driver.findElement(By.className("button radius evo-button large right btnCloseModal global-button2")));
		//wait.until(ExpectedConditions.elementToBeClickable(By.xpath(OR.getProperty("BeneficiariesCloseButton"))));
		//wrapper.checkElementDisplyed(driver.findElement(By.xpath(OR.getProperty("BeneficiariesCloseButton"))));
		//wrapper.click(driver.findElement(By.xpath("BeneficiariesCloseButton")));

		//verify Form link

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[2]/div/p[4]/a/span")));
		wrapper.click(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[2]/div/p[4]/a/span")));
		ATUReports.add(driver.getTitle(),"Clicking on Forms Link","Forms Link clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));

		String winHandleBefore = driver.getWindowHandle();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}



		wrapper.click(driver.findElement(By.id("download")));
		//robot class code will come here
		Robot r=new Robot();



		//Robot class to press ENTER button

		Thread.sleep(5000);
		// wait.until(ExpectedConditions.alertIsPresent());
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

		r.keyPress(KeyEvent.VK_LEFT);
		r.keyRelease(KeyEvent.VK_LEFT);
		driver.close(); 
		//Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		//verify Important Information link

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[8]/div/p[2]/a/span")));
		wrapper.click(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[8]/div/p[2]/a/span")));
		ATUReports.add(driver.getTitle(),"Clicking on Important Information Link","Important Information Link clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));

		//winHandleBefore = driver.getWindowHandle();

		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}




		driver.close(); 
		//Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);


		//verify Privacy Policy link

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[8]/div/p[3]/a/span")));
		wrapper.click(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[8]/div/p[3]/a/span")));
		ATUReports.add(driver.getTitle(),"Clicking on Privacy Policy Link","Privacy Policy Link clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));



		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}



		wrapper.click(driver.findElement(By.id("download")));
		//robot class code will come here
		Thread.sleep(5000);
		// wait.until(ExpectedConditions.alertIsPresent());
		r.keyPress(KeyEvent.VK_ENTER);
		r.keyRelease(KeyEvent.VK_ENTER);

		r.keyPress(KeyEvent.VK_LEFT);
		r.keyRelease(KeyEvent.VK_LEFT);
		driver.close(); 
		//Switch back to original browser (first window)
		driver.switchTo().window(winHandleBefore);

		//Avatar Menu verification

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[1]/img")));
		wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[1]/img")));
		ATUReports.add(driver.getTitle(),"Clicking on Avatar Menu","Avatar Menu Link clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='personalDetails']")));
		wrapper.click(driver.findElement(By.xpath("//*[@id='personalDetails']")));
		ATUReports.add(driver.getTitle(),"Clicking on Personal Details Link","Personal Details Link clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));


		driver.navigate().back();

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[1]/img")));
		wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[1]/img")));

		//Verify Member Number and Last login
		System.out.println(memberNumber);
		wrapper.checkElementDisplyed(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/div/div/div[2]/div/div[2]")));
		wrapper.checkElementDisplyed(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/div/div/div[2]/div/div[3]")));


		// Logout

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/div/div/div[4]/div")));
		wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/div/div/div[4]/div")));

		try{
			//Entering details in Personal Details form




			System.out.println(driver.findElement(By.xpath(OR.getProperty("PersonelConfirmationCheck"))).getText());
			if(driver.findElement(By.xpath(OR.getProperty("PersonelConfirmationCheck"))).getText().equalsIgnoreCase("CONFIRMATION"))
			{
				Assert.assertTrue(true,"Confirmation Message received..");
			}
			else
			{
				Assert.assertFalse(true,"Confirmation Message not received..");
			}
			//	Thread.sleep(7000);https://author-dev2.www.marshinc.net/cf#/content/mercer/global/all/en/our-thinking.html
		}catch(Exception e)
		{
			ATUReports.add(driver.getTitle(),"Error Occured..",""+e, LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
		}

		//SupportiveMethods.closeBrowser(driver);

	}

	/*//Handling all exception that are thrown
	catch(Throwable t)
	{			
		System.out.println("Exception Occured..");	
		SupportiveMethods.logReporter(driver.getTitle(),null,"Unexpected Error Happened..","Unexpected Error happened.."+t,LogResultAs.FAIL);		
	}

	finally
	{
		SupportiveMethods.closeBrowser(driver);	
	}
	 */}

