package testcases;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utilities.Abstract;
import utilities.BaseClass;
import utilities.SupportiveMethods;
import utilities.WrapperFunctions;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;


public class Mercer_PublicPageNavigation_UI_TC009 extends BaseClass {
	static WebDriver driver=null;
	static Properties OR=null;
	List<String>handBurgerMenuNames=new ArrayList<String>();
	List<String>adviceNames=new ArrayList<String>();
	List<String>retirementNames=new ArrayList<String>();
	List<String>mercerMagazineNames=new ArrayList<String>();
	static String testScore="unset";
	WrapperFunctions wrapper;
	List<String>dashboardElementNames=new ArrayList<String>();
	//Set Property for ATU Reporter Configuration
	{
		System.out.println("Getting config files..");
		System.setProperty("atu.reporter.config",Abstract.ATUconfigPath);
		System.out.println("Config read successfull");
	}


	@DataProvider(name="TestDataProvider")
	public Object[][] getData() throws Exception
	{
		//Loading all properties files to POJO
		this.initializeEnvironment();
		String env=getProjectConfig().getProperty("environment");
		String excelpath=Abstract.TestDataPath+"//TestCases"+"_"+env+".xlsx";
		System.out.println("Excel Path: "+excelpath);
		//Passing test case name and test case path for getting TestData
		Object[][] testData = loadDataProvider("Mercer_Australia_UI_Verification_TC011",excelpath,"UI Validation");
		return testData;	
	}


	@Test(dataProvider="TestDataProvider")
	public void mercerHomeNavigation(Hashtable<String,String>testData) throws Exception
	{
		loadTestData(testData);
		//Driver Object
		driver=getDriver();
		OR=SupportiveMethods.ObjectRepository();
		wrapper=new WrapperFunctions(this);

		System.out.println("**********************Ececution STarts Here*********************");
		String browser=dpString("Browser");
		System.out.println("Browser Name: "+dpString("Browser"));	
		System.out.println("Testing site: "+dpString("Link"));
		ATUReports.add("Mercer Australia Secure Site UAT Page", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
		System.out.println("Title: "+driver.getTitle());

		SecurePageLogin.secureLogin_Stage(driver);
		System.out.println("Logged In Successfully...");
		driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
		WebDriverWait wait = new WebDriverWait(driver,300);
		wrapper.click(driver.findElement(By.xpath("//a[text()='ACAST']")));	

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.id("TopazWebPartManager1_twp616000635_txtMemberNumber")).clear();
		driver.findElement(By.id("TopazWebPartManager1_twp616000635_txtMemberNumber")).sendKeys("501");
		//driver.findElement(By.xpath("//*[@id=TopazWebPartManager1_twp616000635_txtMemberNumber]")).clear();
		//driver.findElement(By.xpath("//*[@id=TopazWebPartManager1_twp616000635_txtMemberNumber]")).sendKeys("501");



		driver.findElement(By.name("ctl00$TopazWebPartManager1$twp616000635$ctl03")).click();

		try{
			if(driver.findElement(By.id("sample")).isDisplayed())
			{
				wait.until(ExpectedConditions.elementToBeClickable(By.id("sample")));
				driver.findElement(By.id("sample")).click();
			}
		}catch(Exception e)
		{
			System.out.println("Pop up not displayed...");
		}
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[3]/img")));
		wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[3]/img")));
		//wrapper.click("HandBurgerMenu");

		ATUReports.add(driver.getTitle(),"Clicking on Hand Burger Menu","Hand Burger Menu clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
		//	driver.findElement(By.xpath(OR.getProperty("Beneficiaries"))).click();
		//wrapper.click("Beneficiaries");
		System.out.println(driver.getWindowHandles().size());
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
		}
		System.out.println(driver.getTitle());


		//***********************************************************************************
		//Navigation Scripts
		//HandBurgerMenu
		Actions a=new Actions(driver);
		a.moveToElement(driver.findElement(By.xpath(OR.getProperty("HandBurgerMenu")))).click().perform();
		Thread.sleep(5000);
		List<WebElement>handBurgerMenuElements=driver.findElements(By.xpath(OR.getProperty("HandBurgerMenu")));
		System.out.println("No of Elements: "+handBurgerMenuElements.size());


		String expectedLink=driver.findElement(By.xpath("//a[contains(text(),\"\")]")).getAttribute("href");
		System.out.println("*******************************************************");
		System.out.println("Expected Link: "+expectedLink);
		wrapper.click(driver.findElement(By.xpath("//a[contains(text(),\"\")]")));
		//driver.findElement(By.xpath("//a[contains(text(),\""+superNames.get(i)+"\")]")).click();		
		System.out.println("Title: "+driver.getTitle());
		String actualLink=driver.getCurrentUrl();
		System.out.println("Actual Link: "+driver.getCurrentUrl());
		if(actualLink.contains(expectedLink))
		{
			ATUReports.add(driver.getTitle(),actualLink,expectedLink,LogAs.PASSED,new CaptureScreen(ScreenshotOf.DESKTOP));
			System.out.println("Redirecting Properly");
		}
		else
		{
			ATUReports.add(driver.getTitle(),actualLink,expectedLink,LogAs.WARNING,new CaptureScreen(ScreenshotOf.DESKTOP));
			System.out.println("####Check URL####");
		}
		System.out.println("*******************************************************");
		SupportiveMethods.logReporter(driver.getTitle(),"","Checking For"+driver.getTitle()+"Visual Validation",driver.getTitle()+"validated Successfully",SupportiveMethods.compareScreen(driver.getTitle().replace('|', '_'),browser));




		{


			wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[3]/img")));
			//wrapper.click("HandBurgerMenu");
			ATUReports.add(driver.getTitle(),"Clicking on Hand Burger Menu","Hand Burger Menu clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
			//	driver.findElement(By.xpath(OR.getProperty("Beneficiaries"))).click();

			wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[2]/div[2]/div/div[6]/a[4]")));
			//wrapper.click("Beneficiaries");
			System.out.println(driver.getWindowHandles().size());
			for (String winHandle : driver.getWindowHandles()) {
				driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
			}
			System.out.println(driver.getTitle());
			//driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

			wrapper.checkElementDisplyed(driver.findElement(By.xpath("//*[@id='mainContent']/div/div/div[2]/div/div/div[3]/section[1]/div/h1")));
			ATUReports.add(driver.getTitle(),"Validating Page Header NOMINATE BEBEFICIARIES","Page Header NOMINATE BEBEFICIARIES validated successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));

			wrapper.checkElementDisplyed(driver.findElement(By.xpath(OR.getProperty("BeneficiariesEditButton"))));
			wrapper.click("BeneficiariesEditButton");


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[1]/img")));
			wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[1]/img")));
			ATUReports.add(driver.getTitle(),"Clicking on Avatar Menu","Avatar Menu Link clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='personalDetails']")));
			wrapper.click(driver.findElement(By.xpath("//*[@id='personalDetails']")));
			ATUReports.add(driver.getTitle(),"Clicking on Personal Details Link","Personal Details Link clicked successfully", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));


			driver.navigate().back();

			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[1]/img")));
			wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/a[1]/img")));

			//Verify Member Number and Last login
			//System.out.println(memberNumber);
			wrapper.checkElementDisplyed(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/div/div/div[2]/div/div[2]")));
			wrapper.checkElementDisplyed(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/div/div/div[2]/div/div[3]")));


			wait.until(ExpectedConditions.elementToBeClickable(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/div/div/div[4]/div")));
			wrapper.click(driver.findElement(By.xpath("/html/body/div[2]/div/div/div[1]/div[2]/div/div[1]/header/nav[1]/section[3]/div/div/div[4]/div")));



			//***********************************************************************************
			//Advice Tab Verification
			/*	
			System.out.println("*******************************************************");
			ATUReports.add(driver.getTitle(), LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			SupportiveMethods.logReporter(driver.getTitle(),"","Checking For"+driver.getTitle()+"Visual Validation",driver.getTitle()+"validated Successfully",SupportiveMethods.compareScreen(driver.getTitle().replace('|', '_'),browser));
			SupportiveMethods.waitMethod(OR.getProperty("AdviceTab"));
			a.moveToElement(driver.findElement(By.xpath(OR.getProperty("AdviceTab")))).click().perform();
		}*/
			/*driver.close();
		driver.quit();*/
			//SupportiveMethods.closeBrowser(driver);
		}
	}}
