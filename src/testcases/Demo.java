package testcases;

import org.testng.annotations.*;

import utilities.*;

public class Demo {

	{
		System.out.println("Getting config files..");
		System.setProperty("atu.reporter.config",Abstract.ATUconfigPath);
		System.out.println("Config read successfull");
	}

	@Test
	public void test1()
	{
		System.out.println("Inside test1");
	}

	@Test
	public void test2()
	{
		System.out.println("Inside test2");
	}

	@Test
	public void test3()
	{
		System.out.println("Inside test3");
	}
}
