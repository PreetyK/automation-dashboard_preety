package testData;

import java.io.*;
import java.util.*;

import org.apache.poi.xssf.usermodel.*;

import utilities.*;


public class ExcelReader {
	
	public static ArrayList<String>ReadDriverSuiteExcel() {
		ArrayList<String>Al= new ArrayList<String>();
		XSSFWorkbook Workbook_obj = null;
		XSSFSheet sheet_obj=null;
		try {
			String Schedulersheetpath =Abstract.dir_path+"\\src\\testData\\TestCases_QA.xlsx";
			System.out.println(Schedulersheetpath);
			try{
			FileInputStream FIS = new FileInputStream(new File(Schedulersheetpath));
			System.out.println("Excel file read successfully...");
			Workbook_obj = new XSSFWorkbook(FIS);
			System.out.println("Before reading Excel sheet....");
			sheet_obj = Workbook_obj.getSheet("TestCases");
			System.out.println("Excel sheet read successfully...");
			}catch(Exception e)
			{
				System.out.println("ERROR"+e);
			}
			int Row_count = sheet_obj.getLastRowNum();
			
			System.out.print("Row count: "+Row_count+"\n");
			for (int i = 1;i<=Row_count;i++)
			{
				System.out.println("I value :"+i);
				XSSFRow row_obj = sheet_obj.getRow(i);
				XSSFCell cell_obj = row_obj.getCell(2);
				String Exec_indicator = cell_obj.getStringCellValue();
				System.out.print("Exec indicator: "+Exec_indicator+"\n");
				String Exec_ind = Exec_indicator.trim();
				System.out.println("Trim String: "+Exec_ind+"\n");
				if (Exec_ind.equalsIgnoreCase("Y"))
				{
					XSSFCell cellobj1 = row_obj.getCell(1);
					String Sheetname = cellobj1.getStringCellValue();
					Al.add(Sheetname);
				}
			}
		} catch (Exception e) {
			System.out.println("Exception"+e);
		}
	
		
		return Al;
	}
}
