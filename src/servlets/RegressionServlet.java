package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import utilities.XMLCreator;

/**
 * Servlet implementation class Regression_Servlet
 */
public class RegressionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException 
	{
		// reading the user input    
		String testcase[]=request.getParameterValues("TestCases");
		ArrayList<String>testCases=new ArrayList<String>();
		for(int i=0;i<testcase.length;i++)
		{
			
			testCases.add(testcase[i]);
			//out.println("<li>"+testcase[i]+"</li>");
		}
		System.out.println("Running Testcases....");
		XMLCreator.xmlCreator(testCases);
		System.out.println("***************I'm back in Servlet*********************");
		RequestDispatcher dispatcher = request.getRequestDispatcher("Jsp/index.jsp");
		dispatcher.forward( request, response );
		
	}  

}
