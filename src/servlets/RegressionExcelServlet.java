package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import testData.*;
import utilities.*;

/**
 * Servlet implementation class Regression_Servlet
 */
public class RegressionExcelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	protected void doGet(HttpServletRequest request, 
			HttpServletResponse response) throws ServletException, IOException 
	{
		// reading the user input
		//String color= request.getParameter("color");
		ArrayList<String>Testcase=ExcelReader.ReadDriverSuiteExcel();
		PrintWriter out = response.getWriter();
		out.println("<html><head><title>Servlet Example</title></head><body><center><font color='green'><h1>Mercer.com Regression Suite</h1></font></center><br>");  
		out.println("<form action='regression' method='Get'>");
		for(int i=0;i<Testcase.size();i++)
		{
			//<input type="checkbox" name="vehicle" value="Bike">I have a bike<br>
			out.println("<input type='checkbox' name='TestCases' value='"+Testcase.get(i)+"'>"+Testcase.get(i)+"<br>");
		}
		out.println("<br><br><input type='submit' name='regression' Value='Run'><input type='submit' name='Cancel' Value='Cancel'>");
	}  

}
