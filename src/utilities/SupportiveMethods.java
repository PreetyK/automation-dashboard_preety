package utilities;

import java.awt.image.BufferedImage;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import atu.testng.reports.ATUReports;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class SupportiveMethods extends BaseClass {

	static WebDriver driver=null;
	static SupportiveMethods s=null;
	static Properties projConfig;
	WrapperFunctions wrapper;
	public SupportiveMethods(Pojo pojo)
	{
		driver=pojo.getDriver();
		projConfig =pojo.getProjectConfig();
		wrapper=new WrapperFunctions(this);
	}
	public static Properties ObjectRepository()
	{
		Properties OR_Prop=new Properties();
		FileInputStream in;
		try {
			in = new FileInputStream(new File(Abstract.OR_Path));
			OR_Prop.load(in);
		} catch (Exception e) {
			System.out.println("Exception Occured..."+e);
		}
		return OR_Prop;
	}
	//Scheduler Method
	public static String Snapshot_flg;
	public static ArrayList<String>ReadDriverSuiteExcel() {
		ArrayList<String>Al= new ArrayList<String>();
		XSSFWorkbook Workbook_obj = null;
		try {			
			String env=projConfig.getProperty("environment");
			String Schedulersheetpath =Abstract.TestDataPath+"//TestCases"+"_"+env+".xlsx";		
			System.out.println(Schedulersheetpath);
			FileInputStream FIS = new FileInputStream(Schedulersheetpath);
			Workbook_obj = new XSSFWorkbook(FIS);
			XSSFSheet sheet_obj = Workbook_obj.getSheet("TestCases");
			int Row_count = sheet_obj.getLastRowNum();
			System.out.print("Row count: "+Row_count+"\n");
			for (int i = 1;i<=Row_count;i++)
			{
				System.out.println("I value :"+i);
				XSSFRow row_obj = sheet_obj.getRow(i);
				XSSFCell cell_obj = row_obj.getCell(2);
				String Exec_indicator = cell_obj.getStringCellValue();
				System.out.print("Exec indicator: "+Exec_indicator+"\n");
				String Exec_ind = Exec_indicator.trim();
				System.out.println("Trim String: "+Exec_ind+"\n");
				if (Exec_ind.equalsIgnoreCase("Y"))
				{
					XSSFCell cellobj1 = row_obj.getCell(1);
					String Sheetname = cellobj1.getStringCellValue();
					Al.add(Sheetname);
				}
			}
		} catch (Exception e) {

		} finally {
			try {
				((Closeable) Workbook_obj).close();
			} catch (IOException e) {
				e.printStackTrace();
				System.out.println("Error Occured:"+e);

			}
		}
		return Al;
	}


	//Function to take screen Shot
	public static WebDriver takeScreenShot(WebDriver driver,String name) throws IOException, Exception
	{

		File scrFile = ((TakesScreenshot)driver).
				getScreenshotAs(OutputType.FILE);

		if(name.contains("Base")&& name.contains("Chrome"))
		{
			String base =System.getProperty("user.dir")+"//ScreenShots//BaseLine//Chrome//"+name;
			System.out.println("Screen shot Path: "+base);
			/*ImageIO.write(image, "PNG", new File(
					System.getProperty("user.dir")+"//ScreenShots//BaseLine//"+name));*/
			FileUtils.copyFile(scrFile,new File(base));
		}
		else if(name.contains("Base"))
		{
			String base =System.getProperty("user.dir")+"//ScreenShots//BaseLine//"+name;
			System.out.println("Screen shot Path: "+base);
			/*ImageIO.write(image, "PNG", new File(
					System.getProperty("user.dir")+"//ScreenShots//BaseLine//"+name));*/
			FileUtils.copyFile(scrFile,new File(base));
		}
		else if(name.contains("Current")&& name.contains("Chrome"))
		{
			String current =System.getProperty("user.dir")+"//ScreenShots//Actual//Chrome//"+name;
			System.out.println("Screen shot Path: "+current);
			/*ImageIO.write(image, "PNG", new File(
					System.getProperty("user.dir")+"//ScreenShots//Actual//"+name));*/
			FileUtils.copyFile(scrFile,new File(current));
		}
		else
		{
			String current =System.getProperty("user.dir")+"//ScreenShots//Actual//"+name;
			System.out.println("Screen shot Path: "+current);
			/*ImageIO.write(image, "PNG", new File(
					System.getProperty("user.dir")+"//ScreenShots//Actual//"+name));*/
			FileUtils.copyFile(scrFile,new File(current));
		}
		//Thread.sleep(1000);
		return driver;
	}
	//Compare screen Shot
	public static LogResultAs compareScreen(String name,String browser) throws Exception
	{
		LogResultAs flag=LogResultAs.PASS;
		String imgOriginal;
		String imgToCompareWithOriginal;
		if(browser.contentEquals("Chrome"))
		{
			imgOriginal=System.getProperty("user.dir")+"//ScreenShots//BaseLine//Chrome//"+name+"_"+browser+"_Base.png";
			imgToCompareWithOriginal =System.getProperty("user.dir")+"//ScreenShots//Actual//Chrome//"+name+"_"+browser+"_Current.png";		
			File org=new File(imgOriginal);
			if(!org.exists())
			{
				SupportiveMethods.takeScreenShot(driver,name+"_"+browser+"_Base.png");
			}
			SupportiveMethods.takeScreenShot(driver,name+"_"+browser+"_Current.png");
		}
		else
		{
			imgOriginal =System.getProperty("user.dir")+"//ScreenShots//BaseLine//"+name+"_Base.png";
			imgToCompareWithOriginal = System.getProperty("user.dir")+"//ScreenShots//Actual//"+name+"_Current.png";
			File org=new File(imgOriginal);
			if(!org.exists())
			{
				SupportiveMethods.takeScreenShot(driver,name+"_Base.png");
			}
			SupportiveMethods.takeScreenShot(driver,name+"_Current.png");
		}

		System.out.println(imgOriginal);	
		System.out.println(imgToCompareWithOriginal);
		String imgOutputDifferences =System.getProperty("user.dir")+"//ScreenShots//Error//"+name+"_"+browser+"_Difference.png";
		System.out.println(imgOutputDifferences);
		//Image comparison algorithm
		ImageComparison imageComparison = new ImageComparison(250,250,0.05);
		if(imageComparison.fuzzyEqual(imgOriginal,imgToCompareWithOriginal,imgOutputDifferences))
		{
			System.out.println("Both Images are Equal..");
			flag=LogResultAs.PASS;
		}
		else
		{

			System.out.println("Both Images are not equal..");
			mergerErrorImages(imgOriginal,imgToCompareWithOriginal,imgOutputDifferences);
			flag=LogResultAs.INFO;
		}
		//	Thread.sleep(1000);
		return flag;
	}

	public static void mergerErrorImages(String imgF1,String imgF2,String imgF3) throws IOException
	{
		File imgFile1=new File(imgF1);
		File imgFile2=new File(imgF2);
		File imgFile3=new File(imgF3);

		BufferedImage img1 = ImageIO.read(imgFile1);
		BufferedImage img2 = ImageIO.read(imgFile2);
		BufferedImage img3 = ImageIO.read(imgFile3);

		int widthImg1 = img1.getWidth();
		int heightImg1 = img1.getHeight();

		int widthImg2 = img2.getWidth();
		int heightImg2 = img2.getHeight();

		int widthImg3 = img3.getWidth();
		int heightImg3 = img3.getHeight();

		BufferedImage img = new BufferedImage(
				widthImg1+widthImg2+widthImg3, // Final image will have width and height as
				heightImg1, // addition of widths and heights of the images we already have
				BufferedImage.TYPE_INT_RGB);

		boolean image1Drawn = img.createGraphics().drawImage(img1, 0, 0, null); // 0, 0 are the x and y positions
		if(!image1Drawn) System.out.println("Problems drawing first image"); //where we are placing image1 in final image

		boolean image2Drawn = img.createGraphics().drawImage(img2, widthImg1, 0, null); // here width is mentioned as width of
		if(!image2Drawn) System.out.println("Problems drawing second image"); // image1 so both images will come in same level

		boolean image3Drawn = img.createGraphics().drawImage(img3, widthImg1+widthImg2, 0, null); // here width is mentioned as width of
		if(!image3Drawn) System.out.println("Problems drawing Third image"); // image1 so both images will come in same level

		// horizontally
		File final_image = new File(imgF3); // �png can also be used here�
		boolean final_Image_drawing = ImageIO.write(img, "png", final_image); //if png is used, write �png� instead �jpeg�
		if(!final_Image_drawing)
			System.out.println("Problems drawing final image");
	}



	public static void logReporter(String step, String inputValue, String expectedValue, String actualValue, LogResultAs resultLog)
	{
		String screenShot=projConfig.getProperty("atu.reports.takescreenshot.PassStep");
		if(resultLog == LogResultAs.PASS)
		{

			if(screenShot.equalsIgnoreCase("false")==false)
			{
				ATUReports.add(step, inputValue,expectedValue,actualValue, atu.testng.reports.logging.LogAs.PASSED, null);
			}
			else 
			{
				ATUReports.add(step, inputValue,expectedValue,actualValue, atu.testng.reports.logging.LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}

		else if(resultLog == LogResultAs.INFO)
		{

			ATUReports.add(step, inputValue,expectedValue,actualValue, atu.testng.reports.logging.LogAs.WARNING, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));

		}
		else
		{
			ATUReports.add(step, inputValue,expectedValue,actualValue, atu.testng.reports.logging.LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			Assert.assertTrue(false);
		}

	}

	public static void logReporter(String step, String inputValue, String expectedValue, String actualValue, boolean resultLog)
	{
		String screenShot=projConfig.getProperty("atu.reports.takescreenshot.PassStep");
		System.out.println("result log"+resultLog);
		if(resultLog ==true)
		{

			if(screenShot.equalsIgnoreCase("false")==false)
			{
				ATUReports.add(step, inputValue,expectedValue,actualValue, atu.testng.reports.logging.LogAs.PASSED, null);
			}
			else 
			{
				ATUReports.add(step, inputValue,expectedValue,actualValue, atu.testng.reports.logging.LogAs.PASSED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			}
		}
		else
		{
			ATUReports.add(step, inputValue,expectedValue,actualValue, atu.testng.reports.logging.LogAs.FAILED, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
			Assert.assertTrue(false);
		}

	}



	//Wait method
	public static void waitMethod(String xpath)
	{
		WebElement myDynamicElement = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable((By.xpath(xpath))));
	}
	public static void waitMethodId(String id)
	{
		WebElement myDynamicElement = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable((By.id(id))));
	}
	//Method to find all broken links
	public static List findAllLinks(WebDriver driver)

	{

		List<WebElement> elementList = new ArrayList();

		elementList = driver.findElements(By.tagName("a"));

		elementList.addAll(driver.findElements(By.tagName("img")));

		List finalList = new ArrayList();

		for(WebElement element : elementList)

		{

			if(element.getAttribute("href") != null)

			{

				finalList.add(element);

			}		  

		}	

		return finalList;

	}

	public static String isLinkBroken(URL url) throws Exception

	{

		//url = new URL("http://yahoo.com");

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		try

		{

			connection.connect();

			String response = connection.getResponseMessage();	        

			connection.disconnect();

			return response;

		}

		catch(Exception exp)

		{

			return exp.getMessage();

		}				

	}


}
