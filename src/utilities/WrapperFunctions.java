package utilities;



import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;


public class WrapperFunctions
{
	private WebDriver webDriver;
	private WebDriverWait wait;
	private Hashtable<String, String> testData;
	private Properties projConfig,objRepository;
	private SupportiveMethods objSupportive;
	public String testScore="unset";

	public WrapperFunctions(Pojo objPojo)
	{
		webDriver = objPojo.getDriver();
		wait = objPojo.getWait();
		testData = objPojo.getDataPoolHashTable();
		projConfig = objPojo.getProjectConfig();
		objRepository=objPojo.getObjectRepository();
		//objSupportive = new SupportiveMethods(objPojo);
	}

	/**
	 * @Method: waitForElementToBeClickable
	 * @Description: This is wrapper method wait for element to be click able
	 * @param locator - By identification of element
	 * @param waitInSeconds - wait time  

	 */
	public void waitForElementToBeClickable(By locator, int waitInSeconds) 
	{
		try 
		{
			//Wait<WebDriver> wait = new WebDriverWait(webDriver, waitInSeconds).ignoring(StaleElementReferenceException.class);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
		} 
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	}

	/**
	 * @Method: waitForElementPresence
	 * @Description: This is wrapper method wait for element presence
	 * @param locator - By identification of element
	 * @param waitInSeconds - wait time 

	 */
	public void waitForElementPresence(By locator, int waitInSeconds) 
	{
		try 
		{
			//Wait<WebDriver> wait = new WebDriverWait(webDriver, waitInSeconds).ignoring(StaleElementReferenceException.class);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
			Thread.sleep(5000);
		} 
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	} 

	public void waitForElement(WebElement locator, int waitInSeconds) 
	{
		if (webDriver instanceof JavascriptExecutor) {
			((JavascriptExecutor)webDriver).executeScript("arguments[0].style.border='2px solid red'", locator);
		}
		Wait<WebDriver>wait = new WebDriverWait(webDriver, waitInSeconds).ignoring(StaleElementReferenceException.class);
		wait.until(ExpectedConditions.elementToBeClickable(locator));	
	} 

	/**
	 * @Method: checkElementExistence
	 * @Description: This is wrapper method to check the existence of any web element on the page
	 * @param locator - By identification of element
	 * @param waitInSeconds - wait time 
	 * @return - true if element present  

	 */
	public boolean checkElementExistence(By locator, int sTimeInSecond)
	{
		try 
		{
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			return true;
		}
		catch(Exception exception)
		{
			return false;
		}
	}

	/**
	 * @Method: checkElementDisplyed
	 * @Description: This is wrapper method to check the existence of any web element on the page
	 * @param locator - By identification of element
	 * @param waitInSeconds - wait time 
	 * @return - true if element present  

	 */
	public boolean checkElementDisplyed(WebElement locator)
	{
		try 
		{
			webDriver.manage().timeouts().implicitlyWait(60,TimeUnit.SECONDS);
			waitForElement(locator,2);
			highlight(locator);
			return true;
		}
		catch(Exception exception)
		{
			return false;
		}
	}

	/**
	 * @Method: click
	 * @Description: This is wrapper method to click on web element 
	 * @param locator - By identification of element
	 * @return - true if click successful

	 */
	public boolean click(String locatorKey) 
	{
		try
		{
			WebElement webElement;
			String LocatorValue=objRepository.getProperty(locatorKey);
			System.out.println("Locator value: "+locatorKey);
			if(!LocatorValue.contains("/"))
				webElement = webDriver.findElement(By.id(LocatorValue));
			else
				webElement = webDriver.findElement(By.xpath(LocatorValue));
			//waitForElement(webElement,60);
			highlight(webElement);
			webElement.click();
			Thread.sleep(500);
			return true;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			//  Assert.assertFalse(true);
			return false;
		}
	}

	public boolean click(WebElement locatorKey) 
	{
		waitForElement(locatorKey, 2);
		highlight(locatorKey);
		locatorKey.click();
		/*			JavascriptExecutor executor = (JavascriptExecutor)webDriver;
			executor.executeScript("arguments[0].click();", webElement);*/
		return true;

	}

	public boolean javaScriptClick(WebElement locator) 
	{
		waitForElement(locator, 60);

		try
		{
			WebElement webElement = locator;		
			JavascriptExecutor executor = (JavascriptExecutor)webDriver;
			executor.executeScript("arguments[0].click();", webElement);
			return true;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			//  Assert.assertFalse(true);
			return false;
		}
	}
	/**
	 * @Method: doubleClick
	 * @Description: This is wrapper method used for doubleClick on element
	 * @param locator - By identification of element
	 * @return - true if double click successful

	 */
	public boolean doubleClick(By locator)
	{
		waitForElementPresence(locator, 10);
		WebElement webElement = webDriver.findElement(locator);
		try
		{
			Actions actionBuilder = new Actions(webDriver);
			actionBuilder.doubleClick(webElement).build().perform();
			return true;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: setText
	 * @Description: This is wrapper method to set text for input element
	 * @param locator - By identification of element
	 * @param fieldValue - field value as string 
	 * @return - true if text entered successfully

	 */
	public boolean setText(String locatorKey, String fieldValue) 
	{		
		WebElement webElement;
		String LocatorValue=objRepository.getProperty(locatorKey);
		System.out.println("Locator value: "+locatorKey);
		if(!LocatorValue.contains("/"))
			webElement = webDriver.findElement(By.id(LocatorValue));
		else
			webElement = webDriver.findElement(By.xpath(LocatorValue));
		waitForElement(webElement,5);
		webElement.clear();
		webElement.sendKeys(fieldValue);
		//webElement.sendKeys(Keys.TAB);
		highlight(webElement);
		return true;		
	}
	
	public boolean setText(WebElement webElement, String fieldValue) 
	{		
		waitForElement(webElement,60);
		webElement.clear();
		webElement.sendKeys(fieldValue);
		//webElement.sendKeys(Keys.TAB);
		highlight(webElement);
		return true;		
	}
	public void highlight(WebElement locatorKey)
	{
		if (webDriver instanceof JavascriptExecutor) {
			((JavascriptExecutor)webDriver).executeScript("arguments[0].style.border='3px solid Lime'", locatorKey);
		}
	}
	/*
	void unhighlightLast() 
	{
		JavascriptExecutor js = (JavascriptExecutor)webDriver;
		WebElement lastElem = null;
		String lastBorder = null;

		String SCRIPT_GET_ELEMENT_BORDER;
		String SCRIPT_UNHIGHLIGHT_ELEMENT;

	    if (lastElem != null) 
	    {
	            // if there already is a highlighted element, unhighlight it
	            js.executeScript(SCRIPT_UNHIGHLIGHT_ELEMENT, lastElem, lastBorder);

	    }
	}*/
	/**
	 * @Method: getText
	 * @Description: This is wrapper method to get text form input elements
	 * @param locator - By identification of element
	 * @param textBy - get text by value attribute (set textBy as value)/ by visible text (set textBy as text)
	 * @return - text as string

	 */
	public String getText(By locator, String textBy) 
	{
		waitForElementPresence(locator, 10);
		WebElement webElement = webDriver.findElement(locator);
		try
		{
			String strText = "";
			if(textBy.equalsIgnoreCase("value"))
				strText = webElement.getAttribute("value");
			else if(textBy.equalsIgnoreCase("text"))
				strText = webElement.getText();
			return strText; 
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return null;
		}
	}

	public boolean verifyText(String locatorKey, String verifyText) 
	{
		WebElement webElement;
		String actualText;
		try
		{
			String LocatorValue=objRepository.getProperty(locatorKey);
			System.out.println("Locator value: "+locatorKey);
			if(!LocatorValue.contains("/"))
				webElement = webDriver.findElement(By.id(LocatorValue));
			else
				webElement = webDriver.findElement(By.xpath(LocatorValue));
			webDriver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			waitForElement(webElement, 10);

			actualText=webElement.getText();
			Assert.assertEquals(actualText, verifyText);
			highlight(webElement);
			return true;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	public String getText(String locatorKey) 
	{
		WebElement webElement;
		String actualText="";
		String LocatorValue=objRepository.getProperty(locatorKey);
		System.out.println("Locator value: "+locatorKey);
		if(!LocatorValue.contains("/"))
			webElement = webDriver.findElement(By.id(LocatorValue));
		else
			webElement = webDriver.findElement(By.xpath(LocatorValue));
		webDriver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		waitForElement(webElement, 10);
		actualText=webElement.getText();
		highlight(webElement);
		return actualText;

	}
	/**
	 * @Method: selectCheckBox
	 * @Description: This is wrapper method select/deselect checkbox
	 * @param locator - By identification of element
	 * @param status - select/deselect 

	 */
	public boolean selectCheckBox(By locator, boolean status) 
	{
		waitForElementPresence(locator, 10);
		WebElement webElement = webDriver.findElement(locator);
		try
		{
			if(webElement.getAttribute("type").equals("checkbox"))   
			{
				if((webElement.isSelected() && !status) || (!webElement.isSelected() && status))
					webElement.click();

				return true;
			}
			else
				return false;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: isCheckBoxSelected
	 * @Description: This is wrapper checkbox is selected or not
	 * @param locator - By identification of element

	 */
	public boolean isCheckBoxSelected(By locator, boolean status) 
	{
		waitForElementPresence(locator, 10);
		WebElement webElement = webDriver.findElement(locator);
		boolean state = false;
		try
		{
			if(webElement.getAttribute("type").equals("checkbox"))   
				state = webElement.isSelected();

			return state;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: isRadioButtonSelected
	 * @Description: This is wrapper RadioButton is selected or not
	 * @param locator - By identification of element

	 */
	public boolean isRadioButtonSelected(By locator, boolean status) 
	{
		waitForElementPresence(locator, 10);
		WebElement webElement = webDriver.findElement(locator);
		boolean state = false;
		try
		{
			if(webElement.getAttribute("type").equals("radio"))   
				state = webElement.isSelected();

			return state;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: selectRadioButton
	 * @Description: This is wrapper method select/deselect radio button
	 * @param locator - By identification of element
	 * @param status - select/deselect 

	 */
	public boolean selectRadioButton(By locator, boolean status)
	{
		waitForElementPresence(locator, 10);
		WebElement webElement = webDriver.findElement(locator);
		try
		{
			if(webElement.getAttribute("type").equals("radio"))   
			{
				if((webElement.isSelected() && !status) || (!webElement.isSelected() && status))
					webElement.click();
				return true;
			}
			else
				return false;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: mouseHover
	 * @Description: This is wrapper method used for Mouse Hovering to the element
	 * @param locator - By identification of element
	 *
	 */
	public boolean mouseHover(By locator)
	{
		waitForElementPresence(locator, 10);
		WebElement webElement = webDriver.findElement(locator);
		try
		{
			Actions actionBuilder = new Actions(webDriver);
			actionBuilder.moveToElement(webElement).build().perform();
			return true;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: switchToWindowUsingTitle
	 * @Description: This is wrapper method used switch to window using the given title
	 * @param locator - Window title
	 *
	 */
	public boolean switchToWindowUsingTitle(String windowTitle)
	{
		try
		{
			String mainWindowHandle = webDriver.getWindowHandle();
			Set<String> openWindows = webDriver.getWindowHandles();

			if (!openWindows.isEmpty()) 
			{
				for (String windows : openWindows) 
				{
					String window = webDriver.switchTo().window(windows).getTitle();
					if (windowTitle.equals(window)) 
						return true;
					else 
						webDriver.switchTo().window(mainWindowHandle);
				}
			}
			return false;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: selectDropDownOption
	 * @Description: This is wrapper method select drop down element
	 * @param locator - By identification of element
	 * @param option - drop down element (user may specify text/value/index)
	 * @param selectType - select dorp down element by Text/Value/Index

	 */
	public boolean selectDropDownOption(String locatorKey, String option, String... selectType) 
	{
		try
		{
			WebElement webElement;
			Select sltDropDown;
			String LocatorValue=objRepository.getProperty(locatorKey);
			System.out.println("Locator value: "+locatorKey);
			if(!LocatorValue.contains("/"))
				webElement = webDriver.findElement(By.id(LocatorValue));
			else
				webElement = webDriver.findElement(By.xpath(LocatorValue));
			waitForElement(webElement,5);
			sltDropDown=new Select(webElement);
			if(selectType.length > 0 && !selectType[0].equals(""))
			{
				if(selectType[0].equals("Value"))
				{
					sltDropDown.selectByValue(option);
					highlight(webElement);
				}
				else if(selectType[0].equals("Text")){
					sltDropDown.selectByVisibleText(option);
					highlight(webElement);}
				else if(selectType[0].equals("Index")){
					sltDropDown.selectByIndex(Integer.parseInt(option));
					highlight(webElement);}

				return true;
			}
			else
			{
				// Web elements from dropdown list 
				List<WebElement> options = sltDropDown.getOptions();
				boolean blnOptionAvailable = false;
				int iIndex = 0;
				for(WebElement weOptions : options)  
				{  
					if (weOptions.getText().trim().equals(option))
					{
						sltDropDown.selectByIndex(iIndex);
						blnOptionAvailable = true;
					}
					else
						iIndex++;
					if(blnOptionAvailable)
						break;
				}
				if(blnOptionAvailable)
					return true;
				else
					return false;
			}
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: getSelectedValueFormDropDown
	 * @Description: This is wrapper method select drop down element
	 * @param locator - By identification of element

	 */
	public String getSelectedValueFormDropDown(By locator) 
	{
		try
		{
			waitForElementPresence(locator, 10);
			Select selectDorpDown = new Select(webDriver.findElement(locator));
			String selectedDorpDownValue = selectDorpDown.getFirstSelectedOption().getText();
			return selectedDorpDownValue;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return null;
		}

	}
	/**
	 * @Method: selectRadioButtonForSpecificColumn
	 * @Description: This is wrapper method to select radio button from table with respect to particular column content
	 * @param locator - By identification of element (table with all rows)
	 * @param columnContent - String column content
	 * @columnNumberForRadio - integer column number for radio button

	 */
	public boolean selectRadioButtonForSpecificColumn(By locator, String columnContent, int columnNumberForRadio) 
	{
		try
		{
			waitForElementPresence(locator, 10);
			List<WebElement> weResultTable = webDriver.findElements(locator);
			for(WebElement weRow : weResultTable)
			{
				List<WebElement> weColumns = weRow.findElements(By.xpath(".//td"));
				for(WebElement weColumn : weColumns)
				{
					if(weColumn.getText().trim().equals(columnContent))
					{
						WebElement webElement = weRow.findElement(By.xpath(".//td['" + columnNumberForRadio + "']/input[@type='radio']"));
						webElement.click();
						/*						JavascriptExecutor executor = (JavascriptExecutor)webDriver;
						executor.executeScript("arguments[0].click();", webElement);*/
						webElement.click();
						webElement.click();
						webElement.click();
					}
				}
			}
			return true;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: selectCheckBoxForSpecificColumn
	 * @Description: This is wrapper method to select chechbox from table with respect to particular column content
	 * @param locator - By identification of element (table with all rows)
	 * @param columnContent - String column content
	 * @columnNumberForRadio - integer column number for radio button

	 */
	public boolean selectCheckBoxForSpecificColumn(By locator, String columnContent, int columnNumberForRadio) 
	{

		try
		{
			waitForElementPresence(locator, 10);
			@SuppressWarnings("static-access")
			List<WebElement> weResultTable = webDriver.findElements(locator.xpath(".//tbody/tr"));

			for(WebElement weRow : weResultTable)
			{

				List<WebElement> weColumns = weRow.findElements(By.xpath(".//td"));
				for(WebElement weColumn : weColumns)
				{
					if(weColumn.getText().trim().equals(columnContent))
					{
						WebElement  webElement =weRow.findElement(By.xpath(".//td['" + columnNumberForRadio + "']/span/input[@type='checkbox']"));
						if(webElement.getAttribute("checked") == null)
						{
							webElement.click();
							weRow.findElement(By.xpath(".//td[contains(text(),'"+ columnContent +"')]")).click();

							return true;
						}
						else
						{
							if(weColumn.getText().trim().equals(columnContent))
							{
								//WebElement  webElement2 =weRow.findElement(By.xpath(".//td['" + columnNumberForRadio + "']/span/input[@type='checkbox']"));
								if(webElement.getAttribute("checked") != null)
								{
									// webElement.click();
									// weRow.findElement(By.xpath(".//td[contains(text(),'"+ columnContent +"')]")).click();

									return true;
								}

							}
						}
					}
				}

			}
			return false;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: uncheckCheckBoxForSpecificColumn
	 * @Description: This is wrapper method to uncheck chechbox from table with respect to particular column content
	 * @param locator - By identification of element (table with all rows)
	 * @param columnContent - String column content
	 * @columnNumberForRadio - integer column number for radio button

	 */
	public boolean uncheckCheckBoxForSpecificColumn(By locator, String columnContent, int columnNumberForRadio) 
	{
		try
		{
			waitForElementPresence(locator, 10);

			@SuppressWarnings("static-access")
			List<WebElement> weResultTable = webDriver.findElements(locator.xpath(".//tbody/tr"));

			for(WebElement weRow : weResultTable)
			{
				List<WebElement> weColumns = weRow.findElements(By.xpath(".//td"));
				for(WebElement weColumn : weColumns)
				{
					if(weColumn.getText().trim().equals(columnContent))
					{
						WebElement  webElement =weRow.findElement(By.xpath(".//td['" + columnNumberForRadio + "']/span/input[@type='checkbox']"));
						if(webElement.getAttribute("checked").equals("true"))
						{
							webElement.click();
							weRow.findElement(By.xpath(".//td[contains(text(),'"+ columnContent +"')]")).click();
							//weRow.findElement(By.xpath(".//td[contains(text(),'"+ columnContent +"')]")).click();
							return true;
						}
					}
				}
			}
			return false;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: selectCheckBoxForSpecificRowandColumn
	 * @Description: This is wrapper method to select chechbox from table with respect to particular column content
	 * @param locator - By identification of element (table with all rows)
	 * @param columnContent - String column content
	 * @columnNumberForRadio - integer column number for radio button

	 */
	public boolean selectCheckBoxForSpecificRowandColumn(By locator, String columnContent, int columnNumberForRadio) 
	{
		try
		{
			WebElement weResultTable = webDriver.findElement(locator);
			WebElement  webElement =weResultTable.findElement(By.xpath(".//td['" + columnNumberForRadio + "']/span/input[@type='checkbox']"));
			if(webElement.getAttribute("checked") == null)
			{
				webElement.click();
				weResultTable.findElement(By.xpath(".//td[contains(text(),'"+ columnContent +"')]")).click();

				return true;
			}
			return false;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}


	/**
	 * @Method: verifyTableContent
	 * @Description: it will check given data in whole table
	 * @param locator - By identification of element (table with all rows)
	 * @param columnHeader - String column header
	 * @param ContentToVerify - String Content to be verifyed from excel 

	 */
	public boolean verifyTableContent(By locator, String columnHeader, String ContentToVerify ) 
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		boolean blnverify = false;
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber ++;
			}

			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			for(WebElement weRow : weRows)
			{
				WebElement weExceptedClm = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(columnHeader) + "]"));
				if(weExceptedClm.getText().trim().equals(ContentToVerify))
				{
					blnverify = true;
					return blnverify;
				}
			}
			return blnverify;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: getTableContentContains
	 * @Description: it will check given data in whole table
	 * @param locator - By identification of element (table with all rows)
	 * @param columnHeader - String column header
	 * @param ContentToVerify - String Content to be verifyed from excel 

	 */
	public String getTableContentContains(By locator, String columnHeader, String ContentToVerify ) 
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber ++;
			}

			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			for(WebElement weRow : weRows)
			{
				WebElement weExceptedClm = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(columnHeader) + "]"));
				if(weExceptedClm.getText().trim().contains(ContentToVerify))
				{
					return weExceptedClm.getText().trim();
				}
			}
			return "";
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return "";
		}
	}

	/**
	 * @Method: getTableContent
	 * @Description: it will check given data in whole table
	 * @param locator - By identification of element (table with all rows)
	 * @param columnHeader - String column header
	 * @param ContentToVerify - String Content to be verifyed from excel 

	 */
	public String getTableContent(By locator, String columnHeader, String contentToVerify, String requiredColumn, String... multiColumn) 
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		String requiredContent = "";
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber ++;
			}

			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			for(WebElement weRow : weRows)
			{
				WebElement weExceptedClm = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(columnHeader) + "]"));
				if(weExceptedClm.getText().trim().equals(contentToVerify))
				{
					if(multiColumn != null && multiColumn.length > 2)
					{
						WebElement secondColumn = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(multiColumn[0]) + "]"));
						if(secondColumn.getText().trim().equals(multiColumn[1]))
						{
							WebElement weRequiredClm = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(requiredColumn) + "]"));
							requiredContent = weRequiredClm.getText().trim();
							return requiredContent;
						}
					}
					else
					{
						WebElement weRequiredClm = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(requiredColumn) + "]"));
						requiredContent = weRequiredClm.getText().trim();
						return requiredContent;
					}
				}
			}
			return requiredContent;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return null;
		}
	}

	/**
	 * @Method: verifyTableHeader
	 * @Description: it will check table header of given table
	 * @param locator - By identification of element (table with all rows)
	 * @param columnHeader - String column header

	 */

	public boolean verifyTableHeader(By locator, String columnHeader) 
	{
		boolean blnverify = false;
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText();
				if(strHeader.contains(columnHeader))
					blnverify= true;
			}
			return blnverify;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}


	/**
	 * @Method: verifyNoOfSuscriptionOfUserWithGivenPackage
	 * @Description:check whether the user is having given no of subscription and if he does then it check for its package 
	 * @param locator - By identification of element  

	 */
	public boolean VerifyNoOfSuscriptionOfUserWithGivenPackage(By locator, String columnHeader, String columnContent , int NoOfSuscription , String packageType)
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		int SuscriptionCount = 0;
		boolean blnverify = false;
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber ++;
			}
			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			List<String> SuscrptionNos = new ArrayList<String>(); 
			for(WebElement weRow : weRows)
			{
				SuscrptionNos.add(  weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get("Subscription") + "]")).getText());
				SuscriptionCount++;

			}
			if( SuscriptionCount == NoOfSuscription)
			{
				blnverify = true;
			}
			else
				return false;

			if((blnverify == true))
			{

				int subcount= 0;
				for (String suscription : SuscrptionNos)
				{
					webDriver.findElement(By.linkText(suscription)).click();
					if(webDriver.findElement(By.xpath("//p[contains(text(),'Type')]/following-sibling::select")).getText().contains(packageType))
					{
						subcount ++;
					}
				}
				if(subcount == NoOfSuscription)
					return true;
				else
					return false;
			}

		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
		return false;

	}


	/**
	 * @Method: verifyTableContentAndCheckSelected
	 * @Description: 
	 * @param locator - By identification of element (table with all rows)
	 * @param columnHeader - String column header
	 * @param columnContent - String column content

	 */
	public boolean verifyTableContentAndCheckSelected(By locator, String columnHeader, String columnContent, int checkboxColumnNumber) 
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		boolean blnverify = false;
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber ++;
			}

			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			for(WebElement weRow : weRows)
			{
				WebElement weExceptedClm = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(columnHeader) + "]"));
				if(weExceptedClm.getText().trim().contains(columnContent))
				{
					WebElement weCheckBox = weRow.findElement(By.xpath(".//td[" + checkboxColumnNumber + "]/span/input[@type='checkbox']"));
					boolean blnIsSelected = weCheckBox.isSelected();
					if(blnIsSelected)
					{
						blnverify = true;
					}
				}
			}
			return blnverify;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: getTableRowCount
	 * @Description: return NO of rows preset in table. if rows present return count or else return 0.
	 * @param locator - By identification of table element

	 */
	public int getTableRowCount(By locator) 
	{
		try
		{
			WebElement weResultTable = webDriver.findElement(locator);
			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			return weRows.size();
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return 0;
		}
	} 

	/**
	 * @Method: selectDate
	 * @Description: select given date from datepicker
	 * @param locator - By identification of element (table with all rows) 
	 * @param fieldValue-  Date to be selected in following format "DD-MM-YYYY"

	 */

	public boolean selectDate(By locator , String fieldValue, String... dateFormat)
	{
		try 
		{
			// Local veriable
			String Day = "", Month = "", Year = "";

			// clicking on datepicker btn
			WebElement webElement = webDriver.findElement(locator);
			webElement.click();

			// covert dat inyo 'dd-MM-yyyy' format
			String ConvertdDateFormat =  fieldValue.replaceAll("[:/.,]","-");
			SimpleDateFormat formatter;
			if(dateFormat != null && dateFormat.length > 0)
				formatter = new SimpleDateFormat(dateFormat[0]);
			else
				formatter = new SimpleDateFormat("dd-MM-yyyy");

			Date date =  formatter.parse(ConvertdDateFormat);
			Day = new SimpleDateFormat("d").format(date);
			Month = new SimpleDateFormat("MMM").format(date);
			Year = new SimpleDateFormat("yyyy").format(date);


			// double click title bar of calendar popup
			this.doubleClick(By.className("rcTitle"));

			// Get newly opened year and month table
			WebElement dateWidget = webDriver.findElement(By
					.xpath(".//table[@class = 'RadCalendarMonthView RadCalendarMonthView_Default']"));
			List<WebElement> columns = dateWidget.findElements(By.tagName("td"));
			boolean blnCalender = false;
			do
			{
				int intfromYear = Integer
						.parseInt(webDriver
								.findElement(
										By.xpath("//table[@class='RadCalendarMonthView RadCalendarMonthView_Default']/tbody/tr[1]/td[3]/a"))
								.getText());
				int intToYear = Integer
						.parseInt(webDriver
								.findElement(
										By.xpath("//table[@class='RadCalendarMonthView RadCalendarMonthView_Default']/tbody/tr[5]/td[4]/a"))
								.getText());

				if (Integer.parseInt(Year) >= intfromYear && Integer.parseInt(Year) <= intToYear)
				{
					// Select year
					for (WebElement cell : columns)
					{
						if (cell.getText().equals(Year))
						{
							cell.findElement(By.linkText(Year)).click();
							blnCalender = true;
							break;
						}
					}
				} 
				else if (Integer.parseInt(Year) < intfromYear)
				{
					webDriver
					.findElement(
							By.xpath("//table[@class='RadCalendarMonthView RadCalendarMonthView_Default']/tbody/tr[6]/td[3]/a"))
					.click();
				}
				else if (Integer.parseInt(Year) > intToYear)
				{
					webDriver
					.findElement(
							By.xpath("//table[@class='RadCalendarMonthView RadCalendarMonthView_Default']/tbody/tr[6]/td[4]/a"))
					.click();
				}

				if (blnCalender)
				{
					// Select month
					for (WebElement cell : columns)
					{
						if (cell.getText().equals(Month))
						{
							cell.findElement(By.linkText(Month)).click();
							break;
						}
					}
					// Click ok button
					webDriver
					.findElement(
							By.xpath("//table[@class='RadCalendarMonthView RadCalendarMonthView_Default']/tbody/tr[7]/td/input[@value='OK']"))
					.click();

					// Select day
					WebElement selctday = webDriver.findElement(By.className("rcMainTable"));
					List<WebElement> columnsd = selctday.findElements(By.tagName("td"));

					for (WebElement cell : columnsd)
					{
						if (cell.getText().equals(Day) && (cell.getAttribute("class").equals("") || cell.getAttribute("class").equals("rcWeekend")))
						{
							cell.findElement(By.linkText(Day)).click();
							break;
						}
					}
				}
			} while (blnCalender == false);
			return true;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}//end function


	/**
	 * @Method: dpString
	 * @Description:  this method returns data from the the previously loaded datapool
	 * @param columnHeader - excel file header column name
	 * @return - value for corresponding header 

	 */
	public String dpString(String columnHeader)
	{
		try
		{
			if(this.testData.get(columnHeader) == null)
				return "";
			else
			{

				return this.testData.get(columnHeader);
			}
		}
		catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}//end

	/**

	 * @param locator
	 * @description : check if the Field is enabled if it is then Empty given Field
	 */
	public boolean emptyField(By locator)
	{
		WebElement webElement = webDriver.findElement(locator);
		if(webElement.isEnabled())
		{
			webElement.clear();
			return true;
		}
		else
			return false;
	}//end


	public boolean isElementDesabled(By locator) 
	{
		WebElement webElement = webDriver.findElement(locator);
		try
		{
			return webElement.getAttribute("disabled").equals("true");
		} catch (NullPointerException exception)
		{
			return false;
		}
	}//end

	/**
	 * @method : getAttributeValue

	 * @param locator :  By identification of element
	 * @param AttributeName : Name of attribute Whose value we want
	 */
	public String getAttributeValue(By locator , String AttributeName)
	{
		WebElement webElement=webDriver.findElement(locator);
		String AttributeValue=webElement.getAttribute(AttributeName);
		return AttributeValue;
	}//end

	public List<WebElement> getDDLOptions(By locator)
	{
		WebElement webElement = webDriver.findElement(locator);
		Select sltDropDown = new Select(webElement);
		return sltDropDown.getOptions();
	}//end

	/**
	 *  * @method : getTagName

	 * @param locator
	 * @return TageName for given locator
	 */
	public String getTagName(By locator)
	{
		WebElement webElement = webDriver.findElement(locator);
		return webElement.getTagName();

	}//end

	/**
	 * @Method: elementHighlight
	 * @Description: Highlight element by given id
	 * @param locator - By identification of element 

	 */
	public void elementHighlight(By locator)
	{
		WebElement element = webDriver.findElement(locator);
		for (int i = 0; i < 2; i++) {
			JavascriptExecutor js = (JavascriptExecutor) webDriver;
			js.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, "color: red; border: 3px solid red;");
			//objUtilities.waitFor(2000L);
			js.executeScript(
					"arguments[0].setAttribute('style', arguments[1]);",
					element, "");
		}//end
	}

	/**
	 * @Method: verifyControl
	 * @Description: Verify the Type of Control.
	 * @param locator - By identification of element  
	 * @param controlType-  control type of element e.g dropdown.

	 */
	public boolean verifyControl(By locator,String controlType)
	{
		WebElement webElement=webDriver.findElement(locator);
		if(webElement.getTagName().equalsIgnoreCase(controlType))
			return true;

		return false;
	}


	/**
	 * @Method:  isElementPresentAndEnabled(
	 * @Description: Check whether element is present  or not.
	 * @param locator - By identification of element  

	 */
	public boolean isElementPresentAndEnabled( By locator)
	{
		if(webDriver.findElement(locator).isEnabled())
			return true;
		else
			return false;
	}



	/**
	 * @Method: verifyTableContentForTwoDataOfSameRow
	 * @Description: Verify Table Content.
	 * @param locator - By identification of element  
	 * @param columnHeader - columnHeader of Table

	 */
	public boolean verifyTableContentForTwoDataOfSameRow(By locator, String... columnHeader) 
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		boolean blnverify = false;
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber ++;
			}

			if(columnHeader.length == 4)
			{
				List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
				for(WebElement weRow : weRows)
				{
					WebElement weExceptedClm1 = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(columnHeader[0]) + "]"));
					WebElement weExceptedClm2 = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(columnHeader[2]) + "]"));
					if(weExceptedClm1.getText().trim().contains(columnHeader[1]) && weExceptedClm2.getText().trim().contains(columnHeader[3]))
						blnverify = true;
				}
			}
			return blnverify;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}



	/** Get List of WebElements having same Locator properties */
	public List<WebElement> getListOfWebElements(By locator) 
	{
		return webDriver.findElements(locator);
	}


	/**
	 * @Method: getTableContent
	 * @Description: it will return all data under particular header in whole table
	 * @param locator - By identification of element 
	 * @param columnHeader - String column header
	 * @param columnContent - String column content

	 */

	public ArrayList<String> getTableContent(By locator, String columnHeader) 
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		ArrayList<String> a = new ArrayList<String>();
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber ++;
			}
			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			for(int i=1; i<=weRows.size();i++)
			{
				if(webDriver.findElement(By.xpath("//tr["+i+"]/td[1]/img")).getAttribute("src").contains("PhoneActive.png"))
				{
					WebElement weExceptedClm = webDriver.findElement(By.xpath("//tr["+i+"]/td[" + dataColumnHeader.get(columnHeader) + "]"));
					String data=weExceptedClm.getText().trim();
					a.add(data);
				}
			}
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			// return false;
		}
		return a;
	}


	/**
	 * @Method: verifyTableContentAndClick
	 * @Description: 
	 * @param locator - By identification of element (table with all rows)
	 * @param columnHeader - String column header
	 * @param columnContent - String column content

	 */
	public boolean verifyTableContentAndClick(By locator, String columnHeader, String columnContent, int checkboxColumnNumber) 
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		boolean blnverify = false;
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber ++;
			}

			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			for(WebElement weRow : weRows)
			{
				WebElement weExceptedClm = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(columnHeader) + "]"));
				if(weExceptedClm.getText().trim().contains(columnContent))
				{
					WebElement weCheckBox = weRow.findElement(By.xpath(".//td[" + checkboxColumnNumber + "]"));
					weCheckBox.click();
					blnverify=true;
				}
			}
			return blnverify;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	public  ArrayList<String> getDataFromTable(By locator, String columnHeader)
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		ArrayList<String> a = new ArrayList<String>();
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable;
			weResultTable = webDriver.findElement(locator);



			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));

				intColumnNumber++;
				// return dataColumnHeader;
			}
			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			for(int i=1; i<=weRows.size();i++)
			{
				WebElement weExceptedClm = weResultTable.findElement(By.xpath(".//tbody/tr["+i+"]/td[" + dataColumnHeader.get(columnHeader) + "]"));
				String data=weExceptedClm.getText();
				a.add(data);
			}


		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			// return false;
		}
		return a;
	}





	{

	}
	public boolean selectLastRadioButtoninTable(By locator)
	{
		try
		{
			List<WebElement> weRows = webDriver.findElements(locator);
			int rCount=weRows.size();
			webDriver.findElement(By.xpath("//tr["+rCount+"]/td/input[contains(@value,'MyRadioButton')]")).click();
			return true;
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}
	public boolean isElementPresent(String locatorKey)
	{
		WebElement webElement;
		String LocatorValue=objRepository.getProperty(locatorKey);
		System.out.println("Locator value: "+locatorKey);
		if(!LocatorValue.contains("/"))
			webElement = webDriver.findElement(By.id(LocatorValue));
		else
			webElement = webDriver.findElement(By.xpath(LocatorValue));
		webDriver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		waitForElement(webElement, 10);
		boolean a=true;
		try
		{

			if(webElement.isDisplayed()==true)
			{
				a=true;
			}
			else
			{
				a=false;
			}
		}
		catch(Exception exception)
		{
			exception.printStackTrace(); 
			return false;
		}
		return a;

	}



	/**
	 * @Method: waitForElementPresence
	 * @Description: This is wrapper method wait for element presence
	 * @param locator - By identification of element
	 * @param waitInSeconds - wait time 

	 */
	public void waitForMobileElementPresence(By locator, int waitInSeconds) 
	{
		try 
		{
			Wait<WebDriver> wait = new WebDriverWait(webDriver, waitInSeconds).ignoring

					(StaleElementReferenceException.class);
			wait.until(ExpectedConditions.presenceOfElementLocated(locator));
		} 
		catch(Exception exception)
		{
			exception.printStackTrace();
		}
	} 

	/**
	 * @Method: checkMobileElementExistence
	 * @Description: This is wrapper method to check the existence of any web element on the page
	 * @param locator - By identification of element
	 * @param waitInSeconds - wait time 
	 * @return - true if element present  

	 */
	public boolean checkMobileElementExistence(By locator, int sTimeInSecond)
	{
		try 
		{
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			return true;
		}
		catch(Exception exception)
		{
			return false;
		}
	}

	/**
	 * @Method: mobileSetText
	 * @Description: This is wrapper method to set text for input mobile element 
	 * @param MobileElement - mobile element
	 * @param fieldValue - field value as string 
	 * @return - true if text entered successfully

	 */
	public boolean mobileSetText(By locator, String fieldValue) 
	{
		try
		{
			// replace the text
			waitForMobileElementPresence(locator, 10);
			WebElement webElement = webDriver.findElement(locator);
			webElement.clear();
			webElement.sendKeys(fieldValue);
			return true;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: mobileClick
	 * @Description: This is wrapper method to click on mobile element 
	 * @param MobileElement - mobile element
	 * @return - true if click successful

	 */
	public boolean mobileClick(By locator) 
	{
		try
		{
			waitForMobileElementPresence(locator, 10);
			WebElement webElement = webDriver.findElement(locator);
			webElement.click();
			return true;
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: mobileGetText
	 * @Description: This is wrapper method to get text form mobile elements
	 * @param MobileElement - mobile element
	 * @return - text as string

	 */
	public String mobileGetText(By locator) 
	{
		try
		{
			waitForMobileElementPresence(locator, 10);
			WebElement webElement = webDriver.findElement(locator);
			return webElement.getText(); 
		} 
		catch (Exception exception)
		{
			exception.printStackTrace();
			return null;
		}
	}


	/**
	 * @Method: verifyAllTableHeaderFromTable
	 * @Description: This is wrapper method to verify all the table header given in test data are present in table
	 * @param ColumnNo : No Of Column Header present in table and To Be verifyed.
	 * @param ColumnHeaderName : Coloumn header name from test data excel under Which header names are specified. 
	 * @return - List of column header which are not present if list is empty means all columns are present in table

	 */
	public List<String> verifyAllTableHeaderFromTable(By locator , int ColumnNo ,String ColumnHeaderName )
	{ 
		List<String> colNotPresent =  new ArrayList<String>();
		for (int ColNo = 1 ; ColNo <= ColumnNo ; ColNo++)
		{
			if(this.verifyTableHeader(locator,this.dpString(ColumnHeaderName+ColNo)))
			{
				//dont do anything
			}
			else
			{
				//add unavailable column to the list colNotPresent
				colNotPresent.add(this.dpString(ColumnHeaderName+ColNo));
			}

		}
		return colNotPresent;
	}



	/**
	 * @Method: verifyDateAfterCurrentDate
	 * @Description: This is wrapper method to verify future date is displaying as per given input and no. of days
	 * @return - text as string

	 */
	public boolean verifyDateAfterCurrentDate(String inputString1, String numberOfDays)
	{
		boolean blnverify = false;
		try
		{

			SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DATE, Integer.parseInt(numberOfDays));
			Date d = c.getTime();
			String d1=myFormat.format(d);
			if(inputString1.equals(d1))
			{
				blnverify=true;
			}
			else
			{
				blnverify=false;
			}
			return blnverify;
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: clickFromTable
	 * @Description: To Click the link from Table
	 * @param locator - By identification of element (table with all rows)
	 * @param columnHeaderFirst - String column header
	 * @param ContentToVerifyFirst - String Content to be verifyed from excel 

	 */
	public boolean clickFromTable(By locator, String columnHeaderFirst, String ContentToVerifyFirst, String columnHeaderSecond, String contentToVerifySecond, String headerToClick ) 
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		boolean blnverify = false;
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber ++;
			}

			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			for(WebElement weRow : weRows)
			{
				WebElement weExceptedClm = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(columnHeaderFirst) + "]"));
				WebElement weExceptedSecondClm = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(columnHeaderSecond) + "]"));
				if(weExceptedClm.getText().trim().contains(ContentToVerifyFirst) && weExceptedSecondClm.getText().trim().contains(contentToVerifySecond))
				{
					WebElement webElementToClick = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(headerToClick) + "]"));
					this.elementHighlight(By.xpath(".//td[" + dataColumnHeader.get(headerToClick) + "]"));
					webElementToClick.findElement(By.tagName("a")).click();

					blnverify = true;
					break;
				}
			}
			return blnverify;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	/**
	 * @Method: clickFromTable
	 * @Description: To Click the link from Table
	 * @param locator - By identification of element (table with all rows)
	 * @param columnHeaderFirst - String column header
	 * @param ContentToVerifyFirst - String Content to be verifyed from excel 

	 */	
	public boolean clickFromTable(By locator, String columnHeaderFirst, String ContentToVerifyFirst, String headerToClick ) 
	{
		Hashtable<String , String> dataColumnHeader = new Hashtable<String, String>();
		int intColumnNumber = 1;
		boolean blnverify = false;
		try
		{
			waitForElementPresence(locator, 10);
			WebElement weResultTable = webDriver.findElement(locator);

			List<WebElement> weColumnsHeaders = weResultTable.findElements(By.xpath(".//thead/tr/th"));
			for(WebElement weColumnHeader : weColumnsHeaders)
			{
				String strHeader = weColumnHeader.getText().trim();
				if(!strHeader.equals(""))
					dataColumnHeader.put(strHeader, String.valueOf(intColumnNumber));
				intColumnNumber ++;
			}

			List<WebElement> weRows = weResultTable.findElements(By.xpath(".//tbody/tr"));
			for(WebElement weRow : weRows)
			{
				WebElement weExceptedClm = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(columnHeaderFirst) + "]"));
				if(weExceptedClm.getText().trim().contains(ContentToVerifyFirst))
				{
					WebElement webElementToClick = weRow.findElement(By.xpath(".//td[" + dataColumnHeader.get(headerToClick) + "]"));
					this.elementHighlight(By.xpath(".//td[" + dataColumnHeader.get(headerToClick) + "]"));
					webElementToClick.findElement(By.tagName("a")).click();


					blnverify = true;
					break;
				}
			}
			return blnverify;
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
			return false;
		}
	}

	public  boolean checklnkIspresent(String linktext)
	{
		WebElement hyperlink = webDriver.findElement(By.linkText(linktext));
		if(hyperlink.isDisplayed())
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	/**
	 * @Method      : isTableEmpty(By locator)
	 * @Description : Add reason and comment for status change request.
	 * @param       : table locator

	 * @CreationDate: 29 September 2015
	 * @ModifiedDate:
	 */
	public boolean isTableEmpty(By locator)
	{
		WebElement table = webDriver.findElement(locator);
		List<WebElement> weColumnsHeaders = table.findElements(By.xpath(".//thead/tr/th"));
		// Check for No data header in table
		for(WebElement weColumnHeader : weColumnsHeaders)
		{
			String strHeader = weColumnHeader.getText().trim();
			if(strHeader.equalsIgnoreCase("No data"))
				return true;
		}
		return false;
	}
	/**
	 * @Method      : removeAllServices
	 * @Description : To Remove All Services.
	 * @param       : table locator

	 * @CreationDate: 20 Oct 2015
	 * @ModifiedDate:
	 */
	public boolean removeAllServices()
	{
		//WebElement table = webDriver.findElement(locator);
		By checkBoxList = By.xpath(".//table[@id='ctl00_ContentPlaceHolder1_wrgServices_ctl00']/tbody/tr/td[1]/span/input");

		for (WebElement element : this.getListOfWebElements(checkBoxList)) 
		{
			if(!element.isSelected())
				element.click();
		}
		return true;
	}
	/**
	 * @Method      : checkAllServices
	 * @Description : To Remove All Services.
	 * @param       : table locator

	 * @CreationDate: 20 Oct 2015
	 * @ModifiedDate:
	 */
	public boolean checkAllServices() 
	{

		By checkBoxList = By.xpath(".//table[@id='ctl00_ContentPlaceHolder1_wrgServices_ctl00']/tbody/tr/td[1]/span/input");
		for (WebElement element : this.getListOfWebElements(checkBoxList)) 
		{
			if(!element.isSelected())			
				element.click();
		}
		return true;

	}

	/**
	 * @Method      : removeAllAddons
	 * @Description : To Remove All Services.
	 * @param       : table locator

	 * @CreationDate: 20 Oct 2015
	 * @ModifiedDate:
	 */
	public boolean removeAllAddons()
	{
		//WebElement table = webDriver.findElement(locator);

		By checkBoxList = By.xpath(".//table[@id='ctl00_ContentPlaceHolder1_wrgAddOns_ctl00']/tbody/tr/td[1]/span/input");
		for (WebElement element : this.getListOfWebElements(checkBoxList)) 
		{
			if(element.isSelected())
			{
			}else
			{
				element.click();
			}
		}
		return true;
	}
}



