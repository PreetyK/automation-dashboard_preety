package utilities;



import java.util.Hashtable;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Pojo 
{
	private WebDriverWait wait;
	private WebDriver webDriver;
	private RemoteWebDriver remoteWebDriver;
	private String sessionId="";
	private Properties projConfig,objRepository;
	private String testCaseID = "";
	private String runID = "";
	private String maxRuns = "";
	private String strTestDataFilePath = "";
	private Hashtable<String , String> testdataPoolHashTable;
	private WrapperFunctions objWrapperFunctions;
	private SupportiveMethods 	objSuportiveMethodFunctions;	
	private DesiredCapabilities caps;
	private String memberNumber="";
	
	public String getMemberNumber() {
		return memberNumber;
	}
	public void setMemberNumber(String memberNumber) {
		this.memberNumber = memberNumber;
	}
	public DesiredCapabilities getDesiredCaps() {
		return caps;
	}
	public void setDesiredCaps(DesiredCapabilities caps) {
		this.caps = caps;
	}
	public void setRemoteDriver(RemoteWebDriver remotewebDriver)
	{
		this.remoteWebDriver = remotewebDriver;
	}
	
	public String getRemoteSessionId() {
		return sessionId;
	}
	public void setRemoteSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public RemoteWebDriver getRemoteDriver()
	{
		return remoteWebDriver;
	}

	public void setDriver(WebDriver webDriver)
	{
		this.webDriver = webDriver;
	}
	public WebDriver getDriver()
	{
		return webDriver;
	}
	public void setProjectConfig(Properties projConfig)
	{
		this.projConfig = projConfig;
	}

	public Properties getProjectConfig()
	{
		return projConfig;
	}

	public void setObjectRepository(Properties objRepository)
	{
		this.objRepository = objRepository;
	}

	public Properties getObjectRepository()
	{
		return objRepository;
	}

	public void setWait(WebDriverWait wait)
	{
		this.wait = wait;
	}
	public WebDriverWait getWait()
	{
		return wait;
	}

	//Getter Setter for Test Case ID 
	public String getTestCaseID() 
	{
		return testCaseID;
	}

	public void setTestCaseID(String testCaseID) 
	{
		this.testCaseID = testCaseID;
	}

	// Getter Setter for Test Data File Path
	public String getStrTestDataFilePath()
	{
		return strTestDataFilePath;
	}

	public void setStrTestDataFilePath(String strTestDataFilePath)
	{
		this.strTestDataFilePath = strTestDataFilePath;
	}
	// Getter Setter for Max Runs 
	public String getMaxRuns() 
	{
		return maxRuns;
	}

	public void setMaxRuns(String maxRuns) 
	{
		this.maxRuns = maxRuns;
	}

	//Getter Setter for DataPoolHashtable object instance
	public void setTestDataPoolHashTable(Hashtable<String,String>testdataPoolHashTable)
	{
		this.testdataPoolHashTable = testdataPoolHashTable;
	}

	public Hashtable<String, String>getDataPoolHashTable()
	{
		return testdataPoolHashTable;
	}
	//Getter Setter for Wrapper function object instance
	public WrapperFunctions getObjWrapperFunctions() 
	{
		return objWrapperFunctions;
	}

	public void setSupportiveMethodFunctions(SupportiveMethods objSuportiveMethodFunctions)
	{
		this.objSuportiveMethodFunctions = objSuportiveMethodFunctions;
	}

	//Getter Setter for Wrapper function object instance
	public SupportiveMethods getSupportiveMethodFunctions() 
	{
		return objSuportiveMethodFunctions;
	}

	public void setObjWrapperFunctions(WrapperFunctions objWrapperFunctions)
	{
		this.objWrapperFunctions = objWrapperFunctions;
	}
}
