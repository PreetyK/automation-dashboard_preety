package utilities;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.testng.TestNG;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;


public class XMLCreator {

	public static void xmlCreator(ArrayList<String>testcases)
	{
		try {
			DocumentBuilderFactory dbFactory =
					DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = 
					dbFactory.newDocumentBuilder();
			Document doc = dBuilder.newDocument();
			// root element(Suite)
			Element rootElement = doc.createElement("suite");
			Attr attr = doc.createAttribute("name");
			attr.setValue("Mercer.com Suite");
			rootElement.setAttributeNode(attr);
			doc.appendChild(rootElement);
			for(int i=0;i<testcases.size();i++)
			{
				//  test element
				Element test = doc.createElement("test");
				Attr test_attr = doc.createAttribute("name");
				test_attr.setValue(""+testcases.get(i));
				test.setAttributeNode(test_attr);
				rootElement.appendChild(test);

				//  Classes element
				Element classes = doc.createElement("classes");
				test.appendChild(classes);

				//class element
				Element Class = doc.createElement("class");
				Attr class_attr = doc.createAttribute("name");
				class_attr.setValue("testcases."+testcases.get(i));
				Class.setAttributeNode(class_attr);
				classes.appendChild(Class);
			}

			//Listeners
			Element listeners = doc.createElement("listeners");
			rootElement.appendChild(listeners);
			//Listener 1
			Element listener1 = doc.createElement("listener");
			Attr listener_attr1 = doc.createAttribute("class-name");
			listener_attr1.setValue("atu.testng.reports.listeners.ATUReportsListener");
			listener1.setAttributeNode(listener_attr1);
			listeners.appendChild(listener1);
			//Listener 2
			Element listener2 = doc.createElement("listener");
			Attr listener_attr2 = doc.createAttribute("class-name");
			listener_attr2.setValue("atu.testng.reports.listeners.ConfigurationListener");
			listener2.setAttributeNode(listener_attr2);
			listeners.appendChild(listener2);
			//Listener 3
			Element listener3 = doc.createElement("listener");
			Attr listener_attr3 = doc.createAttribute("class-name");
			listener_attr3.setValue("atu.testng.reports.listeners.MethodListener");
			listener3.setAttributeNode(listener_attr3);
			listeners.appendChild(listener3);

			// write the content into xml file
			TransformerFactory transformerFactory =
					TransformerFactory.newInstance();
			Transformer transformer =
					transformerFactory.newTransformer();
			DOMSource source = new DOMSource(doc);
			StreamResult result =
					new StreamResult(new File(Abstract.dir_path+"\\Scheduler.xml"));
			transformer.transform(source, result);
			// Output to console for testing
			StreamResult consoleResult =
					new StreamResult(System.out);
			transformer.transform(source, consoleResult);


			//Running the xml file
			// Create object of TestNG Class
			TestNG runner=new TestNG();

			// Create a list of String 
			List<String> suitefiles=new ArrayList<String>();

			// Add xml file which you have to execute
			suitefiles.add(Abstract.dir_path+"\\Scheduler.xml");

			// now set xml file for execution
			runner.setTestSuites(suitefiles);

			// finally execute the runner using run method
			runner.run();


		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
