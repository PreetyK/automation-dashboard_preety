package utilities;



import java.io.FileInputStream;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.SkipException;

import atu.testng.reports.ATUReports;

public class BaseClass extends Pojo {

	private String testDataFilePath;
	private WebDriver driver;
	static Properties projConfig,objRepository;
	private WebDriverWait wait;
	private WrapperFunctions objWrapperFunctions;
	private SupportiveMethods 	objSuportiveMethodFunctions;
	private Hashtable<String , String> dataPoolHashTable = new Hashtable<String, String>();
	static DesiredCapabilities caps;
	public void initializeEnvironment() throws Exception
	{

		projConfig = new Properties();
		objRepository = new Properties();
		try 
		{
			projConfig.load(new FileInputStream(Abstract.PROJ_CONFIG_Path));
			this.setProjectConfig(projConfig);
			objRepository.load(new FileInputStream(Abstract.OR_Path));
			this.setObjectRepository(objRepository);
		} 
		catch (Exception exception)
		{
			System.out.println("Exception: Project and Object repository not loaded properly");
			exception.printStackTrace();
		}
	}

	public void initialize(String link) throws Exception
	{
		// Excel operation
		String browser=dpString("BrowserName");
		System.out.println(browser);
		String Link=dpString("Link");
		System.out.println(Link);
		if(browser.equals("Firefox"))
		{
			System.out.println("Starting Firefox Driver");
			this.driver=new FirefoxDriver();  
			wait = new WebDriverWait(driver, Integer.parseInt(projConfig.getProperty("midwait")));
			this.setDriver(driver);
			ATUReports.setWebDriver(driver);
			this.setWait(wait);
			driver.manage().window().maximize();
			driver.get(Link);
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
		}
		else if(browser.equals("Chrome"))
		{
			System.out.println("Inside Chrome");
			System.setProperty("webdriver.chrome.driver",Abstract.ChromePath);
			this.driver=new ChromeDriver();  
			//wait = new WebDriverWait(driver, Integer.parseInt(projConfig.getProperty("midwait")));
			this.setDriver(driver);
			//ATUReports.setWebDriver(driver);
			driver.manage().window().maximize();
			driver.get(Link);
			driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(20,TimeUnit.SECONDS);
		}
		else if(browser.equals("IE"))
		{
			System.out.println("Inside IE Driver");
			DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
			capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true); 
			System.setProperty("webdriver.ie.driver",Abstract.IEPath);
			this.driver = new InternetExplorerDriver(capabilities);
			//this.driver=new InternetExplorerDriver();
			this.setDriver(driver);
			ATUReports.setWebDriver(driver);
			this.setWait(wait);
			driver.manage().window().maximize();
			driver.get(Link);
		}
		else if(browser.equalsIgnoreCase("Ghost"))
		{
			System.out.println("Inside Ghost :-P");
			System.setProperty("phantomjs.binary.path",Abstract.GhostPath);
			//this.driver=new PhantomJSDriver();
			wait = new WebDriverWait(driver, Integer.parseInt(projConfig.getProperty("midwait")));
			this.setDriver(driver);
			ATUReports.setWebDriver(driver);	
			getDriver().get(Link);
		}
		else
		{
			RemoteWebDriver remotewebDriver = null;
			System.out.println("Involking Remote Browser...");
			caps=getCapability(caps);
			//remotewebDriver = new RemoteWebDriver(new URL("http://ondemand.saucelabs.com:80"), caps);
			remotewebDriver = new RemoteWebDriver(new URL("http://" +projConfig.getProperty("username") + ":" + projConfig.getProperty("authkey")+"@ondemand.saucelabs.com:80/wd/hub"), caps);
			wait = new WebDriverWait(remotewebDriver, Integer.parseInt(projConfig.getProperty("midwait")));		
			this.setDriver(remotewebDriver);
			this.setRemoteSessionId(remotewebDriver.getSessionId().toString());
			this.setWait(wait);
			ATUReports.setWebDriver(getDriver());
			//driver.manage().window().maximize();
			getDriver().get(Link);
			//getDriver().manage().window().maximize();
		}		
	}
	public DesiredCapabilities getCapability(DesiredCapabilities caps)
	{
		caps=new DesiredCapabilities();
		caps.setCapability(CapabilityType.BROWSER_NAME, dpString("BrowserName"));
        caps.setCapability(CapabilityType.VERSION, "49.0");
        caps.setCapability(CapabilityType.PLATFORM,"Windows 10");
        caps.setCapability("parentTunnel", "erik-horrell");
        caps.setCapability("tunnelIdentifier", "MercerTunnel1");
        caps.setCapability("screenResolution", "1280x768");
        caps.setCapability("name", getTestCaseID());	
		/*
		caps.setCapability("name",""+getTestCaseID());
		caps.setCapability("build","1.0");
		caps.setCapability("browser_api_name",dpString("BrowserName"));
		caps.setCapability("os_api_name",dpString("OS"));
		caps.setCapability("screen_resolution",dpString("Resolution"));
		caps.setCapability("record_video",projConfig.get("record_video"));
		caps.setCapability("record_network",projConfig.get("record_network"));*/
		this.setDesiredCaps(caps);
		return caps;
	}
	public Object[][] loadDataProvider(String testCaseID,String excelPath,String sheetName)
	{
		Object[][]dataPool=null;
		try{
			ExcelUtil xls=new ExcelUtil(excelPath,sheetName);
			dataPool=TestUtil.getData(testCaseID,xls);
			this.setTestCaseID(testCaseID);
			this.setStrTestDataFilePath(testDataFilePath);
			this.setMaxRuns("Run" + dataPool.length);

		}
		catch(Exception e)
		{
			System.out.println("LoadDataProvide failed to execute..");
		}
		return dataPool;
	}

	public void loadTestData(Hashtable<String,String>dataSet) throws Exception
	{

		this.setTestDataPoolHashTable(dataSet);
		objWrapperFunctions = new WrapperFunctions(this);
		this.setObjWrapperFunctions(objWrapperFunctions);	
		this.initialize(dpString("Link"));
		objSuportiveMethodFunctions=new SupportiveMethods(this);
		this.setSupportiveMethodFunctions(objSuportiveMethodFunctions);
	}

	public void runMode()
	{
		String flag=dpString("Runmode");
		if(flag.equalsIgnoreCase("n"))
		{
			throw new SkipException("Test case skipped");
		}
	}

	public void closeBrowser(WebDriver driver)
	{
		try 
		{
			driver.quit();
		}
		catch (Exception exception)
		{
			exception.printStackTrace();
		}
	}


	public String dpString(String columnHeader)
	{
		dataPoolHashTable = this.getDataPoolHashTable();
		try
		{
			if(dataPoolHashTable.get(columnHeader) == null)
				return "";
			else
				return dataPoolHashTable.get(columnHeader);
		}
		catch (Exception e) 
		{
			throw new RuntimeException(e);
		}
	}

	public void Login()
	{
		driver=getDriver();
		WrapperFunctions objWrapperFunctions=new WrapperFunctions(this);
		SupportiveMethods.logReporter("","","User should able to launch the application","User launched the application successfully",true);
		objWrapperFunctions.setText( driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtCompanyName_txtCompany']")),"100001");
		/* driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtCompanyName_txtCompany']")).clear();
		  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtCompanyName_txtCompany']")).sendKeys("100001");
		 */ SupportiveMethods.logReporter("","","User should able enter the Employer/Plan No","User entered the Employer/Plan no successfully",true);
		 objWrapperFunctions.setText(driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtUserName']")),"2222");	
		 /*		  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtUserName']")).clear();
		  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtUserName']")).sendKeys("2222");
		  */		  SupportiveMethods.logReporter("","","User should able enter the Member number","User entered the Member number successfully",true);

		  objWrapperFunctions.setText( driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtPassword']")),"4004");
		  /*		  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtPassword']")).clear();
		  driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp1783717138_txtPassword']")).sendKeys("4004");
		   */		  SupportiveMethods.logReporter("","","User should able enter the PIN/Password","User entered the PIN/Password successfully",true);


		   objWrapperFunctions.click(driver.findElement(By.xpath("//input[@name='ctl00$TopazWebPartManager1$twp1783717138$ctl01']")));
		   SupportiveMethods.logReporter("","","User should able to click the submit button","User clicked the submit button successfully successfully",true);

		   // driver.findElement(By.xpath("//*[@id='TopazWebPartManager1_twp435268533_hyplnkNewSite']")).click();
		   driver.manage().timeouts().pageLoadTimeout(3000,TimeUnit.MILLISECONDS);
		   driver.manage().timeouts().implicitlyWait(30000,TimeUnit.MILLISECONDS);	  
		   System.out.println("Title:"+driver.getTitle());

		   WebDriverWait wait = new WebDriverWait(driver,60);

		   WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h1[@class='greeting modal-heading1']")));
		   objWrapperFunctions.highlight(element);
	}

}
