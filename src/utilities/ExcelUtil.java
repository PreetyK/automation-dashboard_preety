package utilities;



import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtil {

	private static XSSFSheet ExcelWSheet;
	private static XSSFWorkbook ExcelWBook;
	private static XSSFCell cell;
	private static XSSFRow Row;

	public ExcelUtil(String Path,String SheetName) throws Exception {

		try {

			System.out.println(Path);
			System.out.println(SheetName);
			// Open the Excel file

			FileInputStream ExcelFile = new FileInputStream(Path);
			
			System.out.println("opening file...");
			// Access the required test data sheet
			int a=ExcelFile.available();
			System.out.println(a);
			System.out.println("Preety:Excel file is availabe :Before reading Excel File");
		
			ExcelWBook = new XSSFWorkbook(ExcelFile);
			System.out.println("Open Excel File");
			ExcelWSheet = ExcelWBook.getSheet(SheetName);
			System.out.println("Reading Excel File");
			System.out.println("############################");
		} catch (Exception e){

			System.out.println("Error Happened...");
			throw (e);

		}
	}
	/*public static Object[][] getTableArray(String FilePath, String SheetName) throws Exception {   

		String[][] tabArray = null;

		try {

			FileInputStream ExcelFile = new FileInputStream(FilePath);

			// Access the required test data sheet

			ExcelWBook = new XSSFWorkbook(ExcelFile);

			ExcelWSheet = ExcelWBook.getSheet(SheetName);

			int startRow = 1;

			int startCol = 1;

			int ci,cj;

			int totalRows = ExcelWSheet.getLastRowNum();
			System.out.println("Total Row count:"+totalRows);

			// you can write a function as well to get Column count

			int totalCols =4;

			tabArray=new String[totalRows-1][totalCols];

			ci=0;

			for (int i=startRow;i<totalRows;i++, ci++) {           	   

				cj=0;

				for (int j=startCol;j<=totalCols;j++, cj++){

					tabArray[ci][cj]=getCellData(i,j);

					System.out.println(tabArray[ci][cj]);  

				}

			}

		}

		catch (FileNotFoundException e){

			System.out.println("Could not read the Excel sheet");

			e.printStackTrace();

		}

		catch (IOException e){

			System.out.println("Could not read the Excel sheet");

			e.printStackTrace();

		}

		return(tabArray);

	}*/

	public String getCellData(int RowNum,int ColNum) throws Exception {
		String value="";
		
			cell = ExcelWSheet.getRow(RowNum).getCell(ColNum);

			//handle null
			if(cell!=null) {
	            switch(cell.getCellType()){
	                case Cell.CELL_TYPE_BOOLEAN:
	                    value=String.valueOf(cell.getBooleanCellValue());
	                    break;
	                case Cell.CELL_TYPE_NUMERIC:
	                    value=BigDecimal.valueOf(
	                        cell.getNumericCellValue()).toPlainString();
	                    break;
	                case Cell.CELL_TYPE_STRING:
	                    value=String.valueOf(cell.getStringCellValue());
	                    break;
	                case Cell.CELL_TYPE_FORMULA:
	                    value=String.valueOf(cell.getCellFormula());
	                    break;
	                case Cell.CELL_TYPE_BLANK:
	                    value="";
	                    break;
	            }
	                    }
			
			//System.out.println("value:"+value);
			/*//
			int dataType = cell.getCellType();
			System.out.println("datatype "+dataType);

			if  (dataType == 3) {

				return "";

			}else{

				String CellData = cell.getStringCellValue();
				System.out.println(CellData);
*/
				return value;

			
		

		}

	}