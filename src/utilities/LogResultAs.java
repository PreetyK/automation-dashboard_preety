package utilities;



public enum LogResultAs {
  PASS(0),
  FAIL(1),
  INFO(2),
  CATEGORY(3);
  
  private int value;
  
  private LogResultAs(int value) 
  {
     this.value = value;
  }
  
  public int getValue() 
  {
     return value;
  }
}
