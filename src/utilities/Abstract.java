package utilities;



public class Abstract {

	//public static final String TestLink="http://www.mercer.com.au/";
	//public static final String dir_path="C:\\Users\\preety-kumari\\workspace\\Final_Dashboard_New_UI";
	public static final String dir_path="E://New folder//automation-dashboard_preety";
	
	public static final String ATUconfigPath=dir_path+"//resources//config//atu.properties";
	
	public static final String OR_Path=dir_path+"//src//testcases//OR.properties";
	
	public static final String PROJ_CONFIG_Path=dir_path+"//resources//config//Project_Config.properties";
	
	public static final String TestDataPath=dir_path+"//src//TestData";
	
	public static final String ChromePath=dir_path+"//resources//drivers//chromedriver.exe";
	
	public static final String GhostPath=dir_path+"//resources//drivers//phantomjs.exe";
	
	public static final String IEPath=dir_path+"//resources//drivers//IEDriverServer.exe";
	
	public static final String TestCases="TestCases.xlsx";
}
