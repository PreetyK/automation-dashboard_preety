package utilities;



import java.util.Hashtable;

public class TestUtil {

	public static Object[][]getData(String testcase,ExcelUtil xls) throws Exception
	{
		//Row no on which testcase start
		int testStartRowNum=0;
		while(!(xls.getCellData(testStartRowNum,0).equalsIgnoreCase(testcase)))
		{
			testStartRowNum++;
		}
		System.out.println("Testcase "+testcase+" start at row "+testStartRowNum);
		//total no of rows in testcases
		int dataStartRow=testStartRowNum+2;
		int rows=0;
		while(!(xls.getCellData(dataStartRow+rows,0).equalsIgnoreCase("")))
		{
			rows++;
			System.out.println("Count: "+rows);
		}
		System.out.println("Testcase "+testcase+" total no of rows row "+rows);
		//total no of Columns in testcases
		int dataStartcol=testStartRowNum+1;
		int cols=0;
		while(!(xls.getCellData(dataStartcol,cols).equals("")))
		{

			cols++;
		}
		System.out.println("Testcase "+testcase+" total no of Column"+cols);

		Object testData[][]=new Object[rows][1];
		Hashtable<String,String>table=null;
		//Fetching all data
		int index=0;

		for(int rNum=dataStartRow;rNum<dataStartRow+rows;rNum++)
		{	
			int flag=0;
			table=new Hashtable<String,String>();
			for(int cNum=0;cNum<cols;cNum++)
			{
				String data=xls.getCellData(rNum, cNum);
				String key=xls.getCellData(dataStartcol,cNum);
				String value=xls.getCellData(rNum, cNum);
				if(key.equalsIgnoreCase("Runmode") && value.equalsIgnoreCase("n"))
				{
					flag=1;
					break;
				}
				else{
					System.out.print("***"+data+"***");
					table.put(key,value);
				}
			}
			if(flag!=1)
			{
				System.out.println("Flag is not 1 now");
				testData[index][0]=table;
				index++;
			}
			else{
				System.out.println("Flag is 1 now");
			}
			System.out.println("############################");
		}

		return testData;
	}
}
