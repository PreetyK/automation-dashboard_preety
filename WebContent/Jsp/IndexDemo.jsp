<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Australia Automation dashboard</title>
<!-- <script>
//paste this code under the head tag or in a separate js file.
	// Wait for window load
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
	</script> -->
</head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<style>

body {
	font-family: "Lato", sans-serif;
}

ul.tab {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	border: 1px solid #ccc;
	background-color: #f1f1f1;
}

/* Float the list items side by side */
ul.tab li {
	float: left;
}

/* Style the links inside the list items */
ul.tab li a {
	display: inline-block;
	color: green;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
	transition: 0.3s;
	font-size: 17px;
}

/* Change background color of links on hover */
ul.tab li a:hover {
	background-color: #ddd;
}

/* Create an active/current tablink class */
ul.tab li a:focus, .active {
	background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
	display: none;
	padding: 6px 12px;
	border: 1px solid #ccc;
	border-top: none;
}
</style>
<body>
<div class="se-pre-con"></div>
	<h1 align="center">
		<b> Australia Automation Framework</b>
	</h1>

	<ul class="tab">
		<li><a href="#" class="tablinks"
			onclick="openTest(event, 'Smoke')">Smoke</a></li>
		<li><a href="#" class="tablinks"
			onclick="openTest(event, 'Regression')">Regression</a></li>
		<li><a href="#" class="tablinks" onclick="openTest(event, 'UI')">UI</a></li>
		<li><a href="#" class="tablinks"
			onclick="openTest(event, 'Security')">Security</a></li>
		<li><a href="#" class="tablinks"
			onclick="openTest(event, 'Performance')">Performance</a></li>
		<li><a href="#" class="tablinks"
			onclick="openTest(event, 'WebServices')">Web Services</a></li>
		<li><a href="#" class="tablinks"
			onclick="openTest(event, 'Reports')">Reports</a></li>
	</ul>

	<div id="Smoke" class="tabcontent">
		<h3 align='center'>Smoke Testing</h3>
		<h4>Please select the testcases to run</h4>
		<br>
		<form action="regression">
			<input type="checkbox" name='TestCases' value="Mercer_Aus_Beneficiaries_TC006">Mercer Australia Beneficiaries
			<br> <br> <input type="checkbox" name='TestCases'
				value="Mercer_Contribution_Caps_TC010">Mercer Contribution Caps
			<br> <br> <input type="checkbox" name='TestCases'
				value="Mercer_PublicPageNavigation_UI_TC009">Mercer Australia UI Validation <br>
			<br> <input type='submit' name='regression' Value='Run'>
			<input type='submit' name='Cancel' Value='Cancel'>
		</form>
	</div>

	<div id="Regression" class="tabcontent">
		<h3 align='center'>Regression testing</h3>
		<h4>Please select the testcases to run</h4>

		<%@ page import="java.util.ArrayList"%>
		<%@ page import="testData.ExcelReader"%>

		<%
			ArrayList<String> Testcase = ExcelReader.ReadDriverSuiteExcel();
		%>
		<form action="regression">
			<%
				for (int i = 0; i < Testcase.size(); i++) {
					//<input type="checkbox" name="vehicle" value="Bike">I have a bike<br>
					out.println("<input type='checkbox' name='TestCases' value='" + Testcase.get(i) + "'>" + Testcase.get(i)
							+ "<br><br>");
				}
				out.println(
						"<br><br><input type='submit' name='regression' Value='Run'><input type='submit' name='Cancel' Value='Cancel'>");
			%>
		</form>
	</div>

	<div id="UI" class="tabcontent">
		<h3 align='center'>UI testing</h3>
		<h4>Please select the testcases to run</h4>
		<form action="regression">
			<input type="checkbox" name='TestCases' value="Mercer_PublicPageNavigation_UI_TC009">Mercer Australia UI Validation
			<br> <br> <br> <input type='submit'
				name='regression' Value='Run'> <input type='submit'
				name='Cancel' Value='Cancel'>
		</form>
	</div>

	<div id="Security" class="tabcontent">
		<h3 align='center'>Security testing</h3>
		<h4>Please select the testcases to run</h4>
		<form action="regression">
			<input type="checkbox" name="TestCases" value="security check">Australia Secure Site UAT
			security check<br> <br> <br> <input type='submit'
				name='regression' Value='Run'> <input type='submit'
				name='Cancel' Value='Cancel'>
		</form>

	</div>

	<div id="Performance" class="tabcontent">
		<h3 align='center'>Performance testing</h3>
		<h4>Perform performance tests here</h4>
	</div>

	<div id="WebServices" class="tabcontent">
		<h3 align='center'>Web Services testing</h3>
		<h4>Perform Web Services tests here</h4>
	</div>

<!-- http://localhost:8080/Final_Dashboard/ATU%20Reports/index.html -->
<!-- /Final_Dashboard/ATU Reports/index.html -->

	<div id="Reports" class="tabcontent">
    <object type="text/html" data="http://localhost:8080/Final_Dashboard/ATU%20Reports/index.html" width="1300px" height="750px" style="overflow:auto;border:2px grey">
    </object>
	</div>

<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
	<script>
		function openTest(evt, testName) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(
						" active", "");
			}
			document.getElementById(testName).style.display = "block";
			evt.currentTarget.className += " active";
		}

		function ProgressDestroy() {
			if (document.all) {// Internet Explorer
				progress.className = 'hide';
			} else if (document.layers) {// Netscape
				document.progress.visibility = false;
			} else if (document.getElementById) {// Netscape 6+
				document.getElementById("progress").className = 'hide';
			}
		}
	</script>
</body>
</html>


