<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head><title>Mercer Automation dashboard</title></head>
<style>
body {
	font-family: "Lato", sans-serif;
}

ul.tab {
	list-style-type: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	border: 1px solid #ccc;
	background-color: #f1f1f1;
}

/* Float the list items side by side */
ul.tab li {
	float: left;
}

/* Style the links inside the list items */
ul.tab li a {
	display: inline-block;
	color: green;
	text-align: center;
	padding: 14px 16px;
	text-decoration: none;
	transition: 0.3s;
	font-size: 17px;
}

/* Change background color of links on hover */
ul.tab li a:hover {
	background-color: #ddd;
}

/* Create an active/current tablink class */
ul.tab li a:focus, .active {
	background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
	display: none;
	padding: 6px 12px;
	border: 1px solid #ccc;
	border-top: none;
}
</style>
<body>

	<h1>
		<center>
			<b>Mercer Automation</b>
		</center>
	</h1>

	<ul class="tab">
		<li><a href="#" class="tablinks"
			onclick="openTest(event, 'Smoke')">Smoke</a></li>
		<li><a href="#" class="tablinks"
			onclick="openTest(event, 'Regression')">Regression</a></li>
		<li><a href="#" class="tablinks" onclick="openTest(event, 'UI')">UI</a></li>
		<li><a href="#" class="tablinks"
			onclick="openTest(event, 'Security')">Security</a></li>
		<li><a href="#" class="tablinks"
			onclick="openTest(event, 'Performance')">Performance</a></li>
	</ul>

	<div id="Smoke" class="tabcontent">
		<h3 align='center'>Smoke Testing</h3>
		<h4>Please select the testcases to run</h4><br>
		<form action="regression">
			<input type="checkbox" name='TestCases' value="Harmonise_Login">Hamonise Login<br><br>
			<input type="checkbox" name='TestCases' value="Mercer_Form_SpeakWithConsultant">Mercer Form Validation<br><br>
			<input type="checkbox" name='TestCases' value="Mercer_UI_Validation">Mercer UI Validation		
			<br><br> 
			<input type='submit' name='regression' Value='Run'>
			<input type='submit' name='Cancel' Value='Cancel'>
		</form>
	</div>

	<div id="Regression" class="tabcontent">
		<h3 align='center'>Regression testing</h3>
		<h4>Please select the testcases to run</h4>
		<form action="regression">
			<input type="checkbox" name='TestCases' value="Harmonise_Login">Hamonise Login<br><br>
			<input type="checkbox" name='TestCases' value="Mercer_Form_SpeakWithConsultant">Mercer Form Validation<br><br>
			<input type="checkbox" name='TestCases' value="Mercer_UI_Validation">Mercer UI Validation		
			<br><br>
			<input type='submit' name='regression' Value='Run'>
			<input type='submit' name='Cancel' Value='Cancel'>
		</form>
	</div>

	<div id="UI" class="tabcontent">
		<h3 align='center'>UI testing</h3>
		<h4>Please select the testcases to run</h4>
		<form action="regression">
			<input type="checkbox" name='TestCases' value="Mercer_UI_Validation">Mercer.com UI Verification<br><br><br> 
			<input type='submit' name='regression' Value='Run'>
			<input type='submit' name='Cancel' Value='Cancel'>
		</form>
	</div>

	<div id="Security" class="tabcontent">
		<h3 align='center'>Security testing</h3>
		<h4>Please select the testcases to run</h4>
		<form action="regression">
			<input type="checkbox" name="security check" value="security check">Mercer.com security check<br> <br><br>
			<input type='submit' name='regression' Value='Run'>
			<input type='submit' name='Cancel' Value='Cancel'>
		</form>

	</div>

	<div id="Performance" class="tabcontent">
		<h3 align='center'>Performance testing</h3>
		<h4>Perform performance tests here</h4>
	</div>

	<script>
		function openTest(evt, testName) {
			var i, tabcontent, tablinks;
			tabcontent = document.getElementsByClassName("tabcontent");
			for (i = 0; i < tabcontent.length; i++) {
				tabcontent[i].style.display = "none";
			}
			tablinks = document.getElementsByClassName("tablinks");
			for (i = 0; i < tablinks.length; i++) {
				tablinks[i].className = tablinks[i].className.replace(
						" active", "");
			}
			document.getElementById(testName).style.display = "block";
			evt.currentTarget.className += " active";
		}

		function ProgressDestroy() {
			if (document.all) {// Internet Explorer
				progress.className = 'hide';
			} else if (document.layers) {// Netscape
				document.progress.visibility = false;
			} else if (document.getElementById) {// Netscape 6+
				document.getElementById("progress").className = 'hide';
			}
		}
	</script>

</body>
</html>


